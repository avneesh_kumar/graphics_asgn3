


#ifndef _TRANSFORMER_HPP_
#define _TRANSFORMER_HPP_
#include <cmath>
#include "gl_framework.hpp" 
#include "SOIL/SOIL.h"



class Transformer
{

private:

  GLuint structurelist;
  GLuint structurecolorlist;
  GLuint forearm_coverlist;
  GLuint draw_chest_list;
  GLuint forelegcoverlist;
  GLuint drawbacklist;
  GLuint bootlist;
  GLuint makehindarm_cover_list;
   public:
       GLuint textureID;
       GLuint textureID2;
 /****rotate controller angles for hind arm****/
      float rotatelefthindarm_x;
      float rotatelefthindarm_y;
      float rotatelefthindarm_z;

      /****rotate controller angles for fore arm****/
      float rotateleftforearm;

      /****rotate controller angles for hind arm****/
      float rotaterighthindarm_x;
      float rotaterighthindarm_y;
      float rotaterighthindarm_z;

      /****rotate controller angles for fore arm****/
      float rotaterightforearm;

      /****rotate controller angles for hind leg****/
      float rotatelefthindleg_x;
      float rotatelefthindleg_y;
      float rotatelefthindleg_z;

      float rotateleftarmweapon_x;
      float rotateleftarmweapon_y;
      float rotateleftarmweapon_z;

      float rotaterightarmweapon_x;
      float rotaterightarmweapon_y;
      float rotaterightarmweapon_z;

      float rotateleftboot_x;
      float rotateleftboot_y;
      float rotateleftboot_z;

      float rotaterightboot_x;
      float rotaterightboot_y;
      float rotaterightboot_z;

      


      /****rotate controller angles for fore leg****/
      float rotateleftforeleg;

      /****rotate controller angles for hind leg****/
      float rotaterighthindleg_x;
      float rotaterighthindleg_y;
      float rotaterighthindleg_z;

      /****rotate controller angles for fore leg****/
      float rotaterightforeleg;

      /****rotate controller angles for  neck****/
      float rotateneck_x;
      float rotateneck_y;
      float rotateneck_z;

      float rotatechest_x;
      float rotatechest_y;
      float rotatechest_z;

      float bonescale_x;
      float bonescale_y;
      float bonescale_z;

      float modelscale_x;
      float modelscale_y;
      float modelscale_z;

      // wheel rotate
      float rotateLeftfWheel;
      float rotateLeftbWheel;
      float rotateRightfWheel;
      float rotateRightbWheel;
//GLFW display callback

     void init();
     void drawcube();
     void drawcolorcube();
     void makeforearm_cover();
     void drawchest();
     void drawcylinder();
     void makeleftforearm();
     void makelefthindarm();
     void makerightshoulder();
     void makeleftshoulder();
     void makehindarm_cover();
     void makeboot();
     void makeforeleg_backcover();
     void makethigh_cover();
     void makeforeleg_cover();
     void makechest();
     void makechestframe();
     void drawback();
     void makearmweapon();
     void makerightforeleg();
     void makeleftforeleg();
     void makerightleg();
     void makeleftleg();
     void makerightbuttuck();
     void makeleftbuttuck();
     void makehead();
     void makeneck();
     void makerighthindarm();
     void makerightforearm();
     void makebone();

     void makemodel();
     //void makebone();
};

#endif