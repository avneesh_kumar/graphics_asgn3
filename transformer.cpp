

#include <cmath>
#include <iostream>
#include <cstdlib>
#include <GL/glew.h>
#include <unistd.h>
#include <stdio.h>

#include "transformer.hpp"

// GLuint loadBMP_custom(const char * imagepath){
//     // Data read from the header of the BMP file
//       unsigned char header[54]; // Each BMP file begins by a 54-bytes header
//       unsigned int dataPos;     // Position in the file where the actual data begins
//       unsigned int width, height;
//       unsigned int imageSize;   // = width*height*3
//       // Actual RGB data
//       unsigned char * data;



//       // Open the file
//       FILE * file = fopen(imagepath,"rb");
//       if (!file){
//         printf("Image could not be opened\n"); 
//         return 0;
//       }
//       if ( fread(header, 1, 54, file)!=54 ){ // If not 54 bytes read : problem
//         printf("Not a correct BMP file\n");
//         return false;
//       }

//       if ( header[0]!='B' || header[1]!='M' ){
//         printf("Not a correct BMP file\n");
//         return 0;
//       }

//       dataPos = *(int*)&(header[0x0A]);
//       imageSize = *(int*)&(header[0x22]);
//       width = *(int*)&(header[0x12]);
//       height = *(int*)&(header[0x16]);

//       // Some BMP files are misformatted, guess missing information
//       if (imageSize==0)    
//         imageSize=width*height*3; // 3 : one byte for each Red, Green and Blue component
//       if (dataPos==0)      
//         dataPos=54; // The BMP header is done that way

//       // Create a buffer
//       data = new unsigned char [imageSize];
  
//       // Read the actual data from the file into the buffer
//       fread(data,1,imageSize,file);
       
//       //Everything is in memory now, the file can be closed
//       fclose(file);

//       // Create one OpenGL texture
//     GLuint textureID;
//     glGenTextures(1, &textureID);
     
//     // "Bind" the newly created texture : all future texture functions will modify this texture
//     glBindTexture(GL_TEXTURE_2D, textureID);
     
//     // Give the image to OpenGL
//     glTexImage2D(GL_TEXTURE_2D, 0,GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data);
    
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//     glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
//     return textureID;

// }


// GLuint LoadTexture(const char* TextureName)


// {


//    GLuint Texture;  //variable for texture


//    glGenTextures(1,&Texture); //allocate the memory for texture


//    glBindTexture(GL_TEXTURE_2D,Texture); //Binding the texture



//    if(glfwLoadTexture2D(TextureName, GLFW_BUILD_MIPMAPS_BIT)){


//       glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);


//       glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);


//       return Texture;


//    }else return -1;


// }



void Transformer::init(){
  /****rotate controller angles for hind arm****/
  //GLuint image = loadBMP_custom("/home/jaswant/Documents/fl_graphics/grahics_asgn2/lena512.bmp");
  rotatelefthindarm_x=0.0f;
  rotatelefthindarm_y=0.0f;
  rotatelefthindarm_z=0.0f;

  /****rotate controller angles for fore arm****/
  rotateleftforearm=0.0f;

  /****rotate controller angles for hind arm****/
  rotaterighthindarm_x=0.0f;
  rotaterighthindarm_y=0.0f;
  rotaterighthindarm_z=0.0f;

  /****rotate controller angles for fore arm****/
  rotaterightforearm=0.0f;

  /****rotate controller angles for hind leg****/
  rotatelefthindleg_x=0.0f;
  rotatelefthindleg_y=0.0f;
  rotatelefthindleg_z=0.0f;

  rotateleftarmweapon_x=0.0f;
  rotateleftarmweapon_y=0.0f;
  rotateleftarmweapon_z=0.0f;

  rotaterightarmweapon_x=0.0f;
  rotaterightarmweapon_y=0.0f;
  rotaterightarmweapon_z=0.0f;

  rotateleftboot_x = 0.0f;
  rotateleftboot_y = 0.0f;
  rotateleftboot_z = 0.0f;

  rotaterightboot_x = 0.0f;
  rotaterightboot_y = 0.0f;
  rotaterightboot_z = 0.0f;

  /****rotate controller angles for fore leg****/
  rotateleftforeleg=0.0f;

  /****rotate controller angles for hind leg****/
  rotaterighthindleg_x=0.0f;
  rotaterighthindleg_y=0.0f;
  rotaterighthindleg_z=0.0f;

  /****rotate controller angles for fore leg****/
  rotaterightforeleg=0.0f;

  /****rotate controller angles for  neck****/
  rotateneck_x=0.0f;
  rotateneck_y=0.0f;
  rotateneck_z=0.0f;

  rotatechest_x=0.0f;
  rotatechest_y=0.0f;
  rotatechest_z=0.0f;

// rotate wheel 
  rotateLeftfWheel= 0.0f;
  rotateLeftbWheel= 0.0f;
  rotateRightfWheel= 0.0f;
  rotateRightbWheel= 0.0f;


  bonescale_x = 1.00f;
  bonescale_y = 0.10f;
  bonescale_z = 0.05f;

  modelscale_x = 0.50f;
  modelscale_y = 0.50f;
  modelscale_z = 0.50f;


glGenTextures(1, &textureID);
glBindTexture(GL_TEXTURE_2D, textureID);
  int width, height;
unsigned char* image = SOIL_load_image("logo.png", &width, &height, 0, SOIL_LOAD_RGB);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
              GL_UNSIGNED_BYTE, image);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
SOIL_free_image_data(image);


glGenTextures(1, &textureID2);
glBindTexture(GL_TEXTURE_2D, textureID2);
 
image = SOIL_load_image("red.jpg", &width, &height, 0, SOIL_LOAD_RGB);
glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
              GL_UNSIGNED_BYTE, image);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
SOIL_free_image_data(image);
//glBindTexture(GL_TEXTURE_2D, tex);
  drawcube();
  drawcolorcube();
  makeforearm_cover();
  drawchest();
  drawback();
  makeforeleg_cover();
  makeboot();
  makehindarm_cover();


}




void Transformer::drawcube(){
  structurelist = glGenLists(1);
  //
  glNewList(structurelist,GL_COMPILE);

    //glEnable(GL_TEXTURE_2D);


    glColor3f(1.0,1.0,1.0 );
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, -0.5 );
    glVertex3f(  0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();

    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f(  0.5,  0.5, 0.5 );
    glVertex3f( -0.5,  0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();
     
    // Purple side - RIGHT
    glBegin(GL_POLYGON);

    glVertex3f( 0.5, -0.5, -0.5 );
    glVertex3f( 0.5,  0.5, -0.5 );
    glVertex3f( 0.5,  0.5,  0.5 );
    glVertex3f( 0.5, -0.5,  0.5 );
    glEnd();
     
    // Green side - LEFT
    glBegin(GL_POLYGON);

    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
     
    // Blue side - TOP
    glBegin(GL_POLYGON);

    glVertex3f(  0.5,  0.5,  0.5 );
    glVertex3f(  0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glEnd();
     
    // Red side - BOTTOM
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, -0.5 );
    glVertex3f(  0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
  

  glEndList();
}

void Transformer::drawcolorcube(){
  structurecolorlist = glGenLists(1);
  glNewList(structurecolorlist,GL_COMPILE);

  
    glColor3f(1.0,0.0,0.0 );
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, -0.5 );
    glVertex3f(  0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();

    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f(  0.5,  0.5, 0.5 );
    glVertex3f( -0.5,  0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();
     
    // Purple side - RIGHT
    glBegin(GL_POLYGON);

    glVertex3f( 0.5, -0.5, -0.5 );
    glVertex3f( 0.5,  0.5, -0.5 );
    glVertex3f( 0.5,  0.5,  0.5 );
    glVertex3f( 0.5, -0.5,  0.5 );
    glEnd();
     
    // Green side - LEFT
    glBegin(GL_POLYGON);

    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
     
    // Blue side - TOP
    glBegin(GL_POLYGON);

    glVertex3f(  0.5,  0.5,  0.5 );
    glVertex3f(  0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glEnd();
     
    // Red side - BOTTOM
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, -0.5 );
    glVertex3f(  0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
  

  glEndList();
}



void Transformer::makebone(){
  glScalef(bonescale_x,bonescale_y,bonescale_z);
  glCallList(structurelist);
 


}

void Transformer::makeforearm_cover(){
  forearm_coverlist = glGenLists(1);
  glNewList(forearm_coverlist,GL_COMPILE);

  glBegin(GL_POLYGON);
    glColor3f(0.888,0.888,0.888);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,0.0,0.2);
    glVertex3f(0.0,0.5,0.2);
    glVertex3f(0.0,0.5,0.0);
    glVertex3f(0.0,0.0,0.0);
  glEnd();

   glBegin(GL_POLYGON);
    glColor3f(0.788,0.888,0.888);
    glVertex3f(0.4,0.0,0.0);
    glVertex3f(0.4,0.0,0.2);
    glVertex3f(0.4,0.5,0.2);
    glVertex3f(0.4,0.5,0.0);
    glVertex3f(0.4,0.0,0.0);
  glEnd();

  glBegin(GL_POLYGON);
    glColor3f(0.788,0.888,0.888);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.4,0.0,0.0);
    glVertex3f(0.4,0.4,0.0);
    glVertex3f(0.0,0.4,0.0);
    glVertex3f(0.0,0.4,0.0);
  glEnd();

  glBegin(GL_POLYGON);
    glColor3f(0.788,0.788,0.888);
    glVertex3f(0.0,0.0,0.2);
    glVertex3f(0.4,0.0,0.2);
    glVertex3f(0.4,0.4,0.2);
    glVertex3f(0.0,0.4,0.2);
    glVertex3f(0.0,0.4,0.2);
  glEnd();
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,0.0,0.2);
    glVertex3f(0.4,0.0,0.2);
    glVertex3f(0.4,0.0,0.0);
    glVertex3f(0.0,0.0,0.0);
    
  glEnd();
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.4,0.0);
    glVertex3f(0.0,0.4,0.2);
    glVertex3f(0.4,0.4,0.2);
    glVertex3f(0.4,0.4,0.0);
    glVertex3f(0.0,0.4,0.0);
    
  glEnd();

  glEndList();
}

void Transformer::drawchest(){
  

  
   
    // Green side - LEFT
    //glColor3f( 1.0,  0.5, 0.0 );
  draw_chest_list = glGenLists(1);
  glNewList(draw_chest_list,GL_COMPILE);

   glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
      glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
     glBindTexture(GL_TEXTURE_2D, textureID);
    glBegin(GL_POLYGON);

    glTexCoord3d(1.0f, 1.0f, 0.75);glVertex3f( 0.25, 0.35, 0.75 );
    glTexCoord3d(1.0f, 0.0f,0.75);glVertex3f( -0.25, 0.35,0.75 );
    glTexCoord3d(0.0f, 0.0f,0.75);glVertex3f( -0.25, -0.25,0.75 );
    glTexCoord3d(0.0f, 1.0f, 0.75);glVertex3f( 0.25, -0.25, 0.75 );
    glEnd();
    glDisable(GL_TEXTURE_2D);
    
     
    // Blue side - TOP
    glColor3f( 0.9,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( -0.25, -0.25,0.75 );
    glVertex3f( 0.25, -0.25, 0.75 );
    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();

    
  
     glColor3f( 0.85,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( -0.5,  0.5, 0.5 );//1
    glVertex3f( -0.25, 0.35,0.75 ); //2
    glVertex3f( -0.25, -0.25,0.75 ); //3
    glVertex3f( -0.5,  -0.5, 0.5 );//1
    glEnd();

    

   

    glBegin(GL_POLYGON);
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( 0.25, 0.35,0.75 ); //2
    glVertex3f( 0.25, -0.25,0.75 ); //3
    glVertex3f( 0.5,  -0.5, 0.5 );//1
    glEnd();

   
    glColor3f( 0.8,  0.4, 0.0 );
   
    glBegin(GL_POLYGON);
    glVertex3f( 0.25, 0.35,0.75 ); //3
    glVertex3f( -0.25, 0.35,0.75 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();


    

    glColor3f( 0.9,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( -0.5, 0.5,0.5 ); //3
    glVertex3f( -0.25, 0.35,0.75 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();

  
    glColor3f( 0.9,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( 0.5, 0.5,0.5 ); //3
    glVertex3f( 0.25, 0.35,0.75 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();

  glEndList();
}


void Transformer::drawcylinder(){

float height=0.3;
float PI = 180;
float resolution = 2;
float radius = 0.3;
float i=0.0;
    /* top triangle */
glRotatef(85,1,0,0);
glColor3f(1,  0, 0);
glBegin(GL_TRIANGLE_FAN);
    glVertex3f(0, height, 0);  /* center */
    for (i = 0; i <= 2 * PI; i += resolution)
        glVertex3f(radius * cos(i), height, radius * sin(i));
glEnd();
/* bottom triangle: note: for is in reverse order */

glColor3f(0.9,  0.5, 0.2);

glBegin(GL_TRIANGLE_FAN);
    glVertex3f(0, 0, 0);  /* center */
    for (i = 2 * PI; i >= 0; i -= resolution)
        glVertex3f(radius * cos(i), 0, radius * sin(i));
    /* close the loop back to 0 degrees */
    glVertex3f(radius, height, 0);
glEnd();

/* middle tube */
glColor3f(0.59,0.59,0.59);

glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= 2 * PI; i += resolution)
    {
        glVertex3f(radius * cos(i), 0, radius * sin(i));
        glVertex3f(radius * cos(i), height, radius * sin(i));
    }
    /* close the loop back to zero degrees */
    glVertex3f(radius, 0, 0);
    glVertex3f(radius, height, 0);
glEnd();


}

void Transformer::drawback(){

  // back starts here

  drawbacklist = glGenLists(1);
  glNewList(drawbacklist,GL_COMPILE);

    
  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
      glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
     glBindTexture(GL_TEXTURE_2D, textureID);
    glBegin(GL_POLYGON);
    glTexCoord3d(1.0f, 1.0f, 0.75);glVertex3f( 0.25, 0.0, 0.3 );
    glTexCoord3d(1.0f, 0.0f,0.75);glVertex3f( -0.25, 0.0,0.3 );
    glTexCoord3d(0.0f, 0.0f,0.75);glVertex3f( -0.25, -0.35,0.3 );
    glTexCoord3d(0.0f, 1.0f, 0.75);glVertex3f( 0.25, -0.35, 0.3 );
    glEnd();
  glDisable(GL_TEXTURE_2D);

    glColor3f( 0.0,  0.0, 1 );
    glBegin(GL_POLYGON);
    glVertex3f( -0.25, -0.35,0.3 );
    glVertex3f( 0.25, -0.35, 0.3 );
    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();
   
     glColor3f(0,  0, 0.9);
     glBegin(GL_POLYGON);
    glVertex3f( -0.5,  0.5, 0.5 );//1
    glVertex3f( -0.25, 0.0,0.3 ); //2
    glVertex3f( -0.25, -0.35,0.3 ); //3
    glVertex3f( -0.5,  -0.5, 0.5 );//1
    glEnd();

     
     //glColor3f( 0.888,  0.888, 0.888 );
    glBegin(GL_POLYGON);
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( 0.25, 0.0,0.3 ); //2
    glVertex3f( 0.25, -0.35,0.3 ); //3
    glVertex3f( 0.5,  -0.5, 0.5 );//1
    glEnd();

   
    glColor3f(0,  0.95, 0.0);
    glBegin(GL_POLYGON);
    glVertex3f( 0.25, 0.0,0.3 ); //2
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( -0.5,  0.5, 0.5);//1
    glVertex3f( -0.25, 0.0,0.3 ); //2
    glEnd();

     glColor3f(1,  1, 1);
    glBegin(GL_POLYGON);
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( 0.0,  1.0, 0.5 );//1
    glVertex3f( -0.5,  0.5, 0.5);//1
    glEnd();

    glEndList();
}


void Transformer::makechestframe(){
  
  glDepthRange(0.0f, 1.0f);
  glPushMatrix();

    glScalef(1,1.05,1);
    glCallList(drawbacklist);
    
  glPopMatrix();
  glPushMatrix();

  glScalef(1,1.05,1);
    glCallList(draw_chest_list);
  glPopMatrix();
  
}

void Transformer::makechest(){

 
 glPushMatrix();
 glTranslatef(0,0.2,0);
  glRotatef(180,0,0,1);
   glPushMatrix();
      glRotatef(90,0,0,1);
      makebone();
    glPopMatrix();
    

   glPushMatrix();
      glTranslatef(0.45,-0.5,0);
      makebone();
   glPopMatrix();

   glPushMatrix();
    
      glTranslatef(0.898,0,0);
      glRotatef(90,0,0,1);
      makebone();
   glPopMatrix();
   glPushMatrix();
    
      glScalef(0.5,1,1);
      glTranslatef(0.45,0.7,0);
      glRotatef(30,0,0,1);
      
      makebone();
   glPopMatrix();
   glPushMatrix();
    
      glScalef(0.5,1,1);
      glTranslatef(1.4,0.7,0);
      glRotatef(-30,0,0,1);
      
      makebone();
   glPopMatrix();

   glPushMatrix();
    
      
      glTranslatef(0.45,0.95,0);
      glRotatef(90,0,0,1);
      glScalef(0.2,1,1);
      makebone();
   glPopMatrix();

   glPushMatrix();
    glTranslatef(0.45,-0.03,-0.5);

    makechestframe();
   glPopMatrix();
glPopMatrix();

  
  


}

void Transformer::makeforeleg_cover(){

   forelegcoverlist = glGenLists(1);
  glNewList(forelegcoverlist,GL_COMPILE);


  glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
    glColor3f(   0.888,  0.888, 0.888 );
    glBegin(GL_POLYGON);
    glVertex3f(  -0.3, -1, 0.0 );
    glVertex3f(  -0.3, -1, 0.2 );
    glVertex3f(  -0.3, -0.97, 0.2 );
    glVertex3f(  -0.3, -0.93, 0.17 );
    glVertex3f(  -0.3, -0.4, 0.17 );
    glVertex3f(  -0.3, -0.33, 0.27 );
    glVertex3f(  -0.3, -0.2, 0.10 );
    glVertex3f(  -0.3, -0.0, 0.10 );
    glVertex3f(  -0.3, -0.0, -0.0 );
     glVertex3f(  -0.3, -0.28, 0.0 );
     glVertex3f(  -0.3, -1, 0.0 );
    glEnd();

     glColor3f(   0.788,  0.788, 0.788 );
    glBegin(GL_POLYGON);
    glVertex3f(  0.3, -1, 0.0 );
    glVertex3f(  0.3, -1, 0.2 );
    glVertex3f(  0.3, -0.97, 0.2 );
    glVertex3f(  0.3, -0.93, 0.17 );
    glVertex3f(  0.3, -0.4, 0.17 );
    glVertex3f(  0.3, -0.33, 0.27 );
    glVertex3f(  0.3, -0.2, 0.10 );
    glVertex3f(  0.3, -0.0, 0.10 );
    glVertex3f(  0.3, -0.0, -0.0 );
     glVertex3f(  0.3, -0.28, 0.0 );
     glVertex3f(  0.3, -1, 0.0 );
    glEnd();
     glColor3f(   0.688,  0.788, 0.788 );
    glBegin(GL_POLYGON);
      glVertex3f(  0.3, -1, 0.0 );
      glVertex3f(  0.3, 0, 0.0 );
      glVertex3f(  -0.3, 0, 0.0 );
      glVertex3f(  -0.3, -1, 0.0 );
      
    glEnd();
    
      
      
    glEnd();
     glColor3f(   0.688,  0.788, 0.888 );
     glBegin(GL_POLYGON);
      glVertex3f(  0.3, -0.57, 0.17 );
      glVertex3f(  0.3, -0.33, 0.27 );
      glVertex3f(  -0.3, -0.33, 0.27 );
     glVertex3f(  -0.3, -0.57, 0.17 );
      
      
    glEnd();

    glColor3f(   0.788,  0.888, 0.688 );
    glBegin(GL_POLYGON);
    glVertex3f(  -0.3, -0.33, 0.27 );
    glVertex3f(  -0.3, -0.2, 0.10 );
    glVertex3f(  0.3, -0.2, 0.10 );
    glVertex3f(  0.3, -0.33, 0.27 );
    
      
      
    glEnd();

    glEndList();
  
}

void Transformer::makethigh_cover(){
glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
    glPushMatrix();
        glPushMatrix();
         glScalef(0.3,0.6,0.05);
         glCallList(structurecolorlist);
        glPopMatrix();

         glPushMatrix();
          glTranslatef(0.0,0.0,-0.2);
           glScalef(0.3,0.6,0.05);
           glCallList(structurecolorlist);
         glPopMatrix();

        glPushMatrix();
          glTranslatef(-0.0,-0.30,-0.1);
          glScalef(0.08,0.05,0.15);
          glCallList(structurecolorlist);
        glPopMatrix();

    glPopMatrix();
     

    
  
}

void Transformer::makeforeleg_backcover(){

  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);
  glDepthFunc(GL_LEQUAL);
  glDepthRange(0.0f, 1.0f);

  glPushMatrix();

        glPushMatrix();
         glScalef(0.3,0.6,0.05);
         glCallList(structurelist);
        glPopMatrix();

  glPopMatrix();


}

void Transformer::makeboot(){

bootlist = glGenLists(1);

  glNewList(bootlist,GL_COMPILE);


    glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.0, 0.0 );//
      glVertex3f(  0.0, -0.05, 0.0 );//
      glVertex3f(  0.0, -0.05, 0.1 );//
      glVertex3f(  0.0, -0.2, 0.1 );//
      glVertex3f(  0.0, -0.2, 1 );//
      glVertex3f(  0.0, -0.18, 1 );//
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.0, -0.1, 0.5 );//
      glVertex3f(  0.0, -0.15, 0.5 );//
      glVertex3f(  0.0, -0.15, 0.3 );//
      glVertex3f(  0.0, -0.05, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.0 );
    glEnd();


    glBegin(GL_POLYGON);
      glVertex3f(  0.25, 0.0, 0.0 );//
      glVertex3f(  0.25, -0.05, 0.0 );//
      glVertex3f(  0.25, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.2, 0.1 );//
      glVertex3f(  0.25, -0.2, 1 );//
      glVertex3f(  0.25, -0.18, 1 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.3 );//
      glVertex3f(  0.25, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.00, 0.1 );//
      glVertex3f(  0.25, -0.00, 0.0 );
    glEnd();


    glBegin(GL_POLYGON);
      
      glVertex3f(  0.25, -0.00, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.0 );
      glVertex3f(  0.25, -0.00, 0.0 );

    glEnd();
    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.00, 0.1 );
      glVertex3f(  0.0, -0.00, 0.1 );


    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.15, 0.3 );//
      glVertex3f(  0.25, -0.15, 0.3 );//
      glVertex3f(  0.25, -0.05, 0.1 );
      glVertex3f(  0.0, -0.05, 0.1 );


    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.15, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.3 );
      glVertex3f(  0.0, -0.15, 0.3 );
      

    glEnd();

     glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, -0.1, 0.5 );//
      glVertex3f(  0.25, -0.1, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.5 );
      glVertex3f(  0.0, -0.15, 0.5 );

    glEnd();

    glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.5 );
      glVertex3f(  0.0, -0.1, 0.5 );

    glEnd();

     glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );
      glVertex3f(  0.0, -0.1, 0.95 );

    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.18, 1 );//
      glVertex3f(  0.25, -0.18, 1 );//
      glVertex3f(  0.25, -0.1, 0.95 );
      glVertex3f(  0.0, -0.1, 0.95 );

    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.2, 1 );//
      glVertex3f(  0.25, -0.2, 1 );//
      glVertex3f(  0.25, -0.18, 1 );
      glVertex3f(  0.0, -0.18, 1 );

    glEnd();
    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.2, 0.1 );//
      glVertex3f(  0.25, -0.2, 0.1 );//
      glVertex3f(  0.25, -0.2, 1 );
      glVertex3f(  0.0, -0.2, 1 );

    glEnd();
    
    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.05, 0.1 );
      glVertex3f(  0.25, -0.05, 0.1 );
      glVertex3f(  0.25, -0.2, 0.1 );
      glVertex3f(  0.0, -0.2, 0.1 );

    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.05, 0.0 );
      glVertex3f(  0.25, -0.05, 0.0 );
      glVertex3f(  0.25, -0.05, 0.1 );
      glVertex3f(  0.0, -0.05, 0.1 );

    glEnd();


    glBegin(GL_POLYGON);
       glVertex3f(  0.0, 0.0, 0.0 );
       glVertex3f(  0.25, 0.0, 0.0 );
       glVertex3f(  0.25, -0.05, 0.0 );
       glVertex3f(  0.0, -0.05, 0.0 );

    glEnd();
     
    glEndList();
    
  
}

void Transformer::makehindarm_cover(){
  makehindarm_cover_list = glGenLists(1);
  glNewList(makehindarm_cover_list,GL_COMPILE);
  glColor3f(1,  0, 0);
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.0);//
    glVertex3f(0.0,0.0,0.4);//
    glVertex3f(0.0,0.6,0.4);//
    glVertex3f(0.0,0.6,0.3);//
    glVertex3f(0.0,0.3,0.3);//
    glVertex3f(0.0,0.3,0.2);//
    glVertex3f(0.0,0.6,0.2);//
    glVertex3f(0.0,0.6,0.1);//
    glVertex3f(0.0,0.0,0.0);//
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.5,0.0,0.0);//
    glVertex3f(0.5,0.0,0.4);//
    glVertex3f(0.5,0.6,0.4);//
    glVertex3f(0.5,0.6,0.3);//
    glVertex3f(0.5,0.3,0.3);//
    glVertex3f(0.5,0.3,0.2);//
    glVertex3f(0.5,0.6,0.2);//
    glVertex3f(0.5,0.6,0.1);//
    glVertex3f(0.5,0.0,0.0);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.1);//
    glVertex3f(0.5,0.6,0.1);//
    glVertex3f(0.5,0.0,0.0);
    glVertex3f(0.0,0.0,0.0);
  glEnd();


  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.2);//
    glVertex3f(0.5,0.6,0.2);//
    glVertex3f(0.5,0.6,0.1);
    glVertex3f(0.0,0.6,0.1);
   glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.3,0.2);//
    glVertex3f(0.5,0.3,0.2);//
    glVertex3f(0.5,0.6,0.2);
    glVertex3f(0.0,0.6,0.2);

  glEnd();
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.3,0.3);//
    glVertex3f(0.5,0.3,0.3);//
    glVertex3f(0.5,0.3,0.2);
    glVertex3f(0.0,0.3,0.2);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.3);//
    glVertex3f(0.5,0.6,0.3);//
    glVertex3f(0.5,0.3,0.3);
    glVertex3f(0.0,0.3,0.3);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.4);//
    glVertex3f(0.5,0.6,0.4);//
    glVertex3f(0.5,0.6,0.3);
    glVertex3f(0.0,0.6,0.3);
  glEnd();


  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.5,0.0,0.0);
    glVertex3f(0.5,0.0,0.4);
    glVertex3f(0.0,0.0,0.4);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.4);//
    glVertex3f(0.5,0.0,0.4);//
    glVertex3f(0.5,0.6,0.4);
    glVertex3f(0.0,0.6,0.4);
  glEnd();

  glEndList();
}

void Transformer::makeleftshoulder(){
  glPushMatrix();
    glPushMatrix();
      glScalef(0.2,1,1);
      makebone();
    glPopMatrix();

    
  glPopMatrix();
}

void Transformer::makerightshoulder(){
  glPushMatrix();
    glPushMatrix();
        glRotatef(180,0,0,1);
        glScalef(0.2,1,1);
        makebone();
    glPopMatrix();
    
  glPopMatrix();
}

void Transformer::makelefthindarm(){
  glPushMatrix();
    glPushMatrix();
        glRotatef(-90,0,0,1);
        glScalef(0.7,1,1);
        makebone();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0.13,-0.2,0.1);
      glScalef(0.7,1.0,0.5);
      glRotatef(180,0,1,0);
     glCallList(makehindarm_cover_list);
    glPopMatrix();

    glPushMatrix();
      glTranslatef(-0.55,0.1,0);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
      glRotatef(rotateRightfWheel,1,0,0);
     drawcylinder();
    glPopMatrix();

  glPopMatrix();
}

void Transformer::makeleftforearm(){
  glPushMatrix();
    glPushMatrix();
          glRotatef(-90,0,0,1);
          glScalef(0.7,1,1);
          makebone();
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-0.2,-0.23,0);
      glScalef(1,1.5,0.5);
     glCallList(forearm_coverlist);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-0.2,-0.23,-0.1);
      glScalef(1,1.5,0.5);
     glCallList(forearm_coverlist);
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makerightforearm(){
  glPushMatrix();
    glPushMatrix();
          glRotatef(-90,0,0,1);
          glScalef(0.7,1,1);
          makebone();
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-0.2,-0.23,0);
      glScalef(1,1.5,0.5);
      glCallList(forearm_coverlist);
    glPopMatrix();

     glPushMatrix();
    glTranslatef(-0.2,-0.23,-0.1);
      glScalef(1,1.5,0.5);
     glCallList(forearm_coverlist);
    glPopMatrix();


  glPopMatrix();
}


void Transformer::makerighthindarm(){
  glPushMatrix();
    glPushMatrix();
        glRotatef(-90,0,0,1);
        glScalef(0.7,1,1);
        makebone();
    glPopMatrix();
    glPushMatrix();
     glTranslatef(0.22,-0.2,0.1);
      glScalef(0.7,1.0,0.5);
      glRotatef(180,0,1,0);
      glCallList(makehindarm_cover_list);
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0.24,0.15,0);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
      glRotatef(rotateLeftfWheel,1,0,0);
     drawcylinder();
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makeneck(){

      glPushMatrix();
        glRotatef(90,0,0,1);
        glScalef(0.3,1,1);
        makebone();
      glPopMatrix();


}

void Transformer::makehead(){


      glPushMatrix();
        glTranslatef(0,0.15,0);
        glScalef(0.3,0.3,0.3);
        glCallList(structurelist);
      glPopMatrix();



}

void Transformer::makeleftbuttuck(){
  glPushMatrix();
    
    glScalef(0.5,1.0,1.0);
    makebone();

  glPopMatrix();
  glPushMatrix();
        glTranslatef(-0.20,-0.10,0);
        glRotatef(90,0,0,1);
        glScalef(0.10,1.0, 1.0);
        makebone();
  glPopMatrix();


}

void Transformer::makerightbuttuck(){
  glPushMatrix();
    
    glScalef(0.5,1.0,1.0);
    makebone();

  glPopMatrix();
  glPushMatrix();
        glTranslatef(0.20,-0.10,0);
        glRotatef(90,0,0,1);
        glScalef(0.10,1.0, 1.0);
        makebone();
  glPopMatrix();


}

void Transformer::makeleftleg(){
  glPushMatrix();
    glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1,1.0);
       makebone();
    glPopMatrix();

    glPushMatrix();
      glTranslatef(0,0.15,0.1);
      glScalef(1.9,1,1);
     makethigh_cover();
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makerightleg(){
  glPushMatrix();
    glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1.0,1.0);
       makebone();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0,0.15,0.1);
      glScalef(1.9,1,1);
     makethigh_cover();
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makeleftforeleg(){

  glPushMatrix();
     glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1,1.0);
       makebone();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0,0.3,0.03);
      glScalef(1,0.7,1.0);
     glCallList(forelegcoverlist);
    glPopMatrix();

    glPushMatrix();
      glTranslatef(-0.6,0,0.1);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
       glRotatef(rotateLeftbWheel,1,0,0);
     drawcylinder();
    glPopMatrix();

    glPushMatrix();
      glTranslatef(0,-0.05,-0.05);
      glScalef(1.8,1,1);
     makeforeleg_backcover();
    glPopMatrix();
    
  glPopMatrix();

}
void Transformer::makerightforeleg(){
  glPushMatrix();
     glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1,1.0);
       makebone();
    glPopMatrix();
     glPushMatrix();
      glTranslatef(0,0.3,0.03);
      glScalef(1,0.7,1.0);
     glCallList(forelegcoverlist);
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0.34,0,0.1);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
      glRotatef(rotateRightbWheel,1,0,0);
     drawcylinder();
    glPopMatrix();
     glPushMatrix();
      glTranslatef(0,-0.05,-0.05);
      glScalef(1.8,1,1);
     makeforeleg_backcover();
    glPopMatrix();
  glPopMatrix();
}



void Transformer::makearmweapon(){

  glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
    
    glColor3f(1,0,0);
     glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.0, 0.0 );
      glVertex3f(  0.0, -0.3, 0.2 );
      glVertex3f(0.0,0.0,1.0);
    glEnd();

      //glColor3f(0.888,0.888,0.788);
     glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.05, 0.0 );
      glVertex3f(  0.0, 0.35, 0.2 );
      glVertex3f(0.0,0.05,1.0);
    glEnd();
     //glColor3f(0.888,0.788,0.888);
     glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.1, -0.2 );
      glVertex3f(  0.0, -0.1, -0.2 );
      glVertex3f(0.0,0.025,0.2);
    glEnd();
     ///glColor3f(0.788,0.888,0.888);
     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.0, 0.0 );
      glVertex3f(  0.05, -0.3, 0.2 );
      glVertex3f(0.05,0.0,1.0);
    glEnd();

     //glColor3f(0.688,0.888,0.788);
     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.05, 0.0 );
      glVertex3f(  0.05, 0.35, 0.2 );
      glVertex3f(0.05,0.05,1.0);
    glEnd();
     //glColor3f(0.888,0.688,0.788);
     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.1, -0.2 );
      glVertex3f(  0.05, -0.1, -0.2 );
      glVertex3f(0.05,0.025,0.2);
    glEnd();


  glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, 0.0, 0.0 );
      glVertex3f(  0.05, 0.0, 0.0 );
      glVertex3f(  0.05, -0.3, 0.2 );
      glVertex3f(  0.00, -0.3, 0.2 );
  glEnd();

     glBegin(GL_POLYGON);
      glVertex3f(  0.05, -0.3, 0.2 );
      glVertex3f(  0.00, -0.3, 0.2 );
      glVertex3f(  0.00, 0.0, 1 );
      glVertex3f(  0.05, 0.0, 1 );
    glEnd();

     glBegin(GL_POLYGON);  
      glVertex3f(  0.0, 0.05, 0.0 );
      glVertex3f(  0.05, 0.05, 0.0 );
      glVertex3f(  0.05, 0.35, 0.2 );
      glVertex3f(  0.00, 0.35, 0.2 );
    glEnd();

     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.35, 0.2 );
      glVertex3f(  0.00, 0.35, 0.2 );
      glVertex3f(  0.00, 0.05, 1 );
      glVertex3f(  0.05, 0.05, 1 );
    glEnd();


    glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.1, -0.2 );
      glVertex3f(  0.05, 0.1, -0.2 );
      glVertex3f(  0.05, -0.1, -0.2 );
      glVertex3f(  0.0, -0.1, -0.2 );

    glEnd();

    glBegin(GL_POLYGON);
     glVertex3f(  0.0, -0.1, -0.2 );
     glVertex3f(  0.05, -0.1, -0.2 );
     glVertex3f(0.05,0.025,0.2);
     glVertex3f(0.0,0.025,0.2);

    glEnd();

    glBegin(GL_POLYGON);
     glVertex3f(0.0,0.025,0.2);
     glVertex3f(0.05,0.025,0.2);
     glVertex3f(  0.05, 0.1, -0.2 );
     glVertex3f(  0.0, 0.1, -0.2 );

    glEnd();

    glTranslatef(-0.01,0.1,-0.26);
    glScalef(0.15,0.3,0.15);
    glCallList(structurecolorlist);




     

    
  
}


void Transformer::makemodel(){
  
  glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glEnable(GL_COLOR_MATERIAL);
glDepthRange(0.0f, 1.0f);



  glPushMatrix();
    glScalef(modelscale_x,modelscale_y,modelscale_z);
    glPushMatrix();
      
      glTranslatef(-0.45,-0.8,0);
      glRotatef(rotatechest_x,1,0,0);
      glRotatef(rotatechest_y,0,1,0);
      glRotatef(rotatechest_z,0,0,1);
      glTranslatef(0.45,0.8,0);
      makechest();

      glPushMatrix();
        glTranslatef(-0.43,0.86,0);
        glTranslatef(0.0,-0.2,0);
          glRotatef(rotateneck_x,1,0,0);
          glRotatef(rotateneck_y,0,1,0);
          glRotatef(rotateneck_z,0,0,1);
        glTranslatef(-0.0, 0.2,0);
        makeneck();

        glPushMatrix();
         
          glScalef(1,1,0.5);
          
          makehead();
           GLfloat lightColor3[] = {1, 0, 0, 1.0f};
           GLfloat lightPos3[] = {1,1,1, 0.0f};
           GLfloat spot_direction[] = {0.0, 0.0, 1.0};

        
           glLightf(GL_LIGHT3,GL_SPOT_CUTOFF,60.0f);
           
         //  // Fairly shiny spot
          glLightf(GL_LIGHT3,GL_SPOT_EXPONENT,100.0f);
          glLightfv(GL_LIGHT3, GL_DIFFUSE, lightColor3);
          glLightfv(GL_LIGHT3, GL_POSITION, lightPos3);
          glEnable(GL_LIGHT3);
        glPopMatrix();

      glPopMatrix();


      glPushMatrix();
        glTranslatef(-1,0.7,0);
        makeleftshoulder();
        glPushMatrix();
          glTranslatef(-0.1,-0.3,0);
          glTranslatef(0.0,0.25,0);
          glRotatef(rotatelefthindarm_x,1,0,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotatelefthindarm_y,0,1,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotatelefthindarm_z,0,0,1);
          glTranslatef(0.0,-0.25,0);


          makelefthindarm();
          glPushMatrix();

            glTranslatef(0.0,-0.7,0);
            glRotatef(0,0,0,1);

            glTranslatef(0.0,0.34,0);
            glRotatef(-10+rotateleftforearm,1,0,0);
            glTranslatef(0.0,-0.34,0);  

            makeleftforearm();

            glPushMatrix();
              glTranslatef(0,-0.5,0.25);
              glScalef(1,1,0.8);
                glTranslatef(0,0.25,-0.25);
                 glRotatef(rotateleftarmweapon_x,1,0,0);
                 glRotatef(rotateleftarmweapon_y,0,1,0);
                 glRotatef(rotateleftarmweapon_z,0,0,1);
                glTranslatef(0,-0.25,0.25);
              makearmweapon();
            glPopMatrix();
          glPopMatrix();

        glPopMatrix();

      glPopMatrix();




      glPushMatrix();
        glTranslatef(0.1,0.7,0);
        makerightshoulder();
        glPushMatrix();
          glTranslatef(0.1,-0.3,0);


          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindarm_x,1,0,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindarm_y,0,1,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindarm_z,0,0,1);
          glTranslatef(0.0,-0.25,0);
          makerighthindarm();


          glPushMatrix();

            glTranslatef(0.0,-0.7,0);
            glRotatef(0,0,0,1);
            glTranslatef(0.0,0.34,0);
            glRotatef(rotaterightforearm,1,0,0);
            glTranslatef(0.0,-0.34,0);  
            makerightforearm();
               glPushMatrix();
                glTranslatef(0,-0.5,0.25);
                glScalef(1,1,0.8);
                  glTranslatef(0,0.25,-0.25);
                 glRotatef(rotaterightarmweapon_x,1,0,0);
                 glRotatef(rotaterightarmweapon_y,0,1,0);
                 glRotatef(rotaterightarmweapon_z,0,0,1);
                 glTranslatef(0,-0.25,0.25);
                makearmweapon();
              glPopMatrix();
          glPopMatrix();

        glPopMatrix();

      glPopMatrix();
    
      glPopMatrix();  

    glPushMatrix();
        glTranslatef(-0.20,-0.82,0);
        makerightbuttuck();
        glPushMatrix();
          glTranslatef(0.2,-0.4,0);

          glTranslatef(0.2,0.25,0);
          glRotatef(rotaterighthindleg_x,1,0,0);
          glTranslatef(-0.2,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindleg_z,0,0,1);
          glTranslatef(0.0,-0.25,0);

          makerightleg();
          glPushMatrix();
            glTranslatef(0.0,-0.5,0);
            glTranslatef(-0.91,0.25,0);
            glRotatef(rotaterightforeleg,1,0,0);
            glTranslatef(0.91,-0.25,0);
            makerightforeleg();

            glPushMatrix();
              glScalef(2.4,1.5,0.8);
              glTranslatef(-0.12,-0.2,-0.3);
               glRotatef(rotaterightboot_x,1,0,0);
               glRotatef(rotaterightboot_y,0,1,0);
              glCallList(bootlist);
            glPopMatrix();
          glPopMatrix();
        glPopMatrix();


      glPopMatrix();


  glPushMatrix();
        glTranslatef(-0.70,-0.82,0);
        makeleftbuttuck();
        glPushMatrix();
          glTranslatef(-0.2,-0.4,0);
          glTranslatef(-0.2,0.25,0);
          glRotatef(rotatelefthindleg_x,1,0,0);
          glTranslatef(0.2,-0.25,0);

          glTranslatef(-0.0,0.25,0);
          
          glRotatef(rotatelefthindleg_z,0,0,1);
          
          glTranslatef(0.0,-0.25,0);

          makeleftleg();
          glPushMatrix();
            glTranslatef(0.0,-0.5,0);
            glTranslatef(0.91,0.25,0);
            glRotatef(rotateleftforeleg,1,0,0);
            glTranslatef(-0.91,-0.25,0);
            makeleftforeleg();
            glPushMatrix();
              glScalef(2.4,1.5,0.8);
              glTranslatef(-0.12,-0.2,-0.3);
               glRotatef(rotateleftboot_x,1,0,0);
               glRotatef(rotateleftboot_y,0,1,0);
              glCallList(bootlist);
            glPopMatrix();
          glPopMatrix();
        glPopMatrix();
        


      glPopMatrix();

      


  glPopMatrix();

}