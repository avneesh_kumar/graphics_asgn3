/*
 * CS475/CS675 - Computer Graphics
 *  ToyLOGO Assignment Base Code
 *
 * Copyright 2009-2014, Parag Chaudhuri, Department of CSE, IIT Bombay
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <string>
#include <math.h>
#include "gl_framework.hpp"
//#include <GL/glut.h>

#define bust      1 
#define pelvis      2
#define right_arm   3
#define left_arm    4
#define right_thigh   5
#define left_thigh    6
#define right_forearm 7
#define left_forearm  8
#define right_leg   9
#define left_leg    10
#define head      11



 float rotate_x=0.0f;
 float rotate_y=0.0f;


 typedef enum{
  none,
  neck,
  left_fore_leg,
  left_hind_leg,
  right_fore_leg,
  right_hind_leg,
  left_fore_arm,
  left_hind_arm,
  right_fore_arm,
  right_hind_arm,
  chest,
  left_boot,
  right_boot

 }keycontrollers;

//GLFW display callback

 GLuint structurelist;

 GLuint structurecolorlist;

 keycontrollers keyctrl = none; 

/****rotate controller angles for hind arm****/
float rotatelefthindarm_x=0.0f;
float rotatelefthindarm_y=0.0f;
float rotatelefthindarm_z=0.0f;

/****rotate controller angles for fore arm****/
float rotateleftforearm=0.0f;

/****rotate controller angles for hind arm****/
float rotaterighthindarm_x=0.0f;
float rotaterighthindarm_y=0.0f;
float rotaterighthindarm_z=0.0f;

/****rotate controller angles for fore arm****/
float rotaterightforearm=0.0f;

/****rotate controller angles for hind leg****/
float rotatelefthindleg_x=0.0f;
float rotatelefthindleg_y=0.0f;
float rotatelefthindleg_z=0.0f;


/****rotate controller angles for fore leg****/
float rotateleftforeleg=0.0f;

/****rotate controller angles for hind leg****/   
float rotaterighthindleg_x=0.0f;
float rotaterighthindleg_y=0.0f;
float rotaterighthindleg_z=0.0f;

/****rotate controller angles for fore leg****/
float rotaterightforeleg=0.0f;

/****rotate controller angles for  neck****/
float rotateneck_x=0.0f;
float rotateneck_y=0.0f;
float rotateneck_z=0.0f;

float rotatechest_x=0.0f;
float rotatechest_y=0.0f;
float rotatechest_z=0.0f;

float bonescale_x = 1.00f;
float bonescale_y = 0.10f;
float bonescale_z = 0.05f;

float modelscale_x = 0.50f;
float modelscale_y = 0.50f;
float modelscale_z = 1.00f;

class Transformer
{
   public:
   	 void init();
     void drawcube();
     void drawcolorcube(float r,float g,float b);
     void makeforearm_cover();
     void drawchest();
     void drawcylinder();
     void makeleftforearm();
     void makelefthindarm();
     void makerightshoulder();
     void makeleftshoulder();
     void makehindarm_cover();
     void makeboot();
     void makeforeleg_backcover();
     void makethigh_cover();
     void makeforeleg_cover();
     void makechest();
     void makechestframe();
     void drawback();
     void makearmweapon();
     void makerightforeleg();
     void makeleftforeleg();
     void makerightleg();
     void makeleftleg();
     void makerightbuttuck();
     void makeleftbuttuck();
     void makehead();
     void makeneck();
     void makerighthindarm();
     void makerightforearm();
     void makebone();

     void makemodel();
     //void makebone();
};



void Transformer::drawcube(){
  structurelist = glGenLists(1);
  glNewList(structurelist,GL_COMPILE);

  
    glColor3f(   1.0,  1.0, 1.0 );
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f(  0.5,  0.5, 0.5 );
    glVertex3f( -0.5,  0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();
     
    // Purple side - RIGHT
    glBegin(GL_POLYGON);

    glVertex3f( 0.5, -0.5, -0.5 );
    glVertex3f( 0.5,  0.5, -0.5 );
    glVertex3f( 0.5,  0.5,  0.5 );
    glVertex3f( 0.5, -0.5,  0.5 );
    glEnd();
     
    // Green side - LEFT
    glBegin(GL_POLYGON);

    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
     
    // Blue side - TOP
    glBegin(GL_POLYGON);

    glVertex3f(  0.5,  0.5,  0.5 );
    glVertex3f(  0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glEnd();
     
    // Red side - BOTTOM
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, -0.5 );
    glVertex3f(  0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
  

  glEndList();
}

void Transformer::drawcolorcube(float r,float g,float b){
  structurecolorlist = glGenLists(1);
  glNewList(structurecolorlist,GL_COMPILE);

  
    glColor3f(   r,  g, b );
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f(  0.5,  0.5, 0.5 );
    glVertex3f( -0.5,  0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();
     
    // Purple side - RIGHT
    glBegin(GL_POLYGON);

    glVertex3f( 0.5, -0.5, -0.5 );
    glVertex3f( 0.5,  0.5, -0.5 );
    glVertex3f( 0.5,  0.5,  0.5 );
    glVertex3f( 0.5, -0.5,  0.5 );
    glEnd();
     
    // Green side - LEFT
    glBegin(GL_POLYGON);

    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
     
    // Blue side - TOP
    glBegin(GL_POLYGON);

    glVertex3f(  0.5,  0.5,  0.5 );
    glVertex3f(  0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5, -0.5 );
    glVertex3f( -0.5,  0.5,  0.5 );
    glEnd();
     
    // Red side - BOTTOM
    glBegin(GL_POLYGON);

    glVertex3f(  0.5, -0.5, -0.5 );
    glVertex3f(  0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5,  0.5 );
    glVertex3f( -0.5, -0.5, -0.5 );
    glEnd();
  

  glEndList();
}


void Transformer::init(){
  drawcube();
  drawcolorcube(1,0,0);

}

void Transformer::makebone(){
  glScalef(bonescale_x,bonescale_y,bonescale_z);
  glCallList(structurelist);
  glEndList();


}

void Transformer::makeforearm_cover(){

  glBegin(GL_POLYGON);
    glColor3f(0.888,0.888,0.888);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,0.0,0.2);
    glVertex3f(0.0,0.5,0.2);
    glVertex3f(0.0,0.5,0.0);
    glVertex3f(0.0,0.0,0.0);
  glEnd();

   glBegin(GL_POLYGON);
    glColor3f(0.788,0.888,0.888);
    glVertex3f(0.4,0.0,0.0);
    glVertex3f(0.4,0.0,0.2);
    glVertex3f(0.4,0.5,0.2);
    glVertex3f(0.4,0.5,0.0);
    glVertex3f(0.4,0.0,0.0);
  glEnd();

  glBegin(GL_POLYGON);
    glColor3f(0.788,0.888,0.888);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.4,0.0,0.0);
    glVertex3f(0.4,0.4,0.0);
    glVertex3f(0.0,0.4,0.0);
    glVertex3f(0.0,0.4,0.0);
  glEnd();

  glBegin(GL_POLYGON);
    glColor3f(0.788,0.788,0.888);
    glVertex3f(0.0,0.0,0.2);
    glVertex3f(0.4,0.0,0.2);
    glVertex3f(0.4,0.4,0.2);
    glVertex3f(0.0,0.4,0.2);
    glVertex3f(0.0,0.4,0.2);
  glEnd();
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.0,0.0,0.2);
    glVertex3f(0.4,0.0,0.2);
    glVertex3f(0.4,0.0,0.0);
    glVertex3f(0.0,0.0,0.0);
    
  glEnd();
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.4,0.0);
    glVertex3f(0.0,0.4,0.2);
    glVertex3f(0.4,0.4,0.2);
    glVertex3f(0.4,0.4,0.0);
    glVertex3f(0.0,0.4,0.0);
    
  glEnd();


  


}

void Transformer::drawchest(){
  

  
   
    // Green side - LEFT
    //glColor3f( 1.0,  0.5, 0.0 );
   glColor3f( 1.0,  0.0, 0.0 );
    glBegin(GL_POLYGON);
    glVertex3f( 0.25, 0.35, 0.75 );
    glVertex3f( -0.25, 0.35,0.75 );
    glVertex3f( -0.25, -0.25,0.75 );
    glVertex3f( 0.25, -0.25, 0.75 );
    glEnd();
    
     
    // Blue side - TOP
    glColor3f( 0.9,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( -0.25, -0.25,0.75 );
    glVertex3f( 0.25, -0.25, 0.75 );
    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();

    
  
     glColor3f( 0.85,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( -0.5,  0.5, 0.5 );//1
    glVertex3f( -0.25, 0.35,0.75 ); //2
    glVertex3f( -0.25, -0.25,0.75 ); //3
    glVertex3f( -0.5,  -0.5, 0.5 );//1
    glEnd();

    

   

    glBegin(GL_POLYGON);
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( 0.25, 0.35,0.75 ); //2
    glVertex3f( 0.25, -0.25,0.75 ); //3
    glVertex3f( 0.5,  -0.5, 0.5 );//1
    glEnd();

   
    glColor3f( 0.8,  0.4, 0.0 );
   
    glBegin(GL_POLYGON);
    glVertex3f( 0.25, 0.35,0.75 ); //3
    glVertex3f( -0.25, 0.35,0.75 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();


    

    glColor3f( 0.9,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( -0.5, 0.5,0.5 ); //3
    glVertex3f( -0.25, 0.35,0.75 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();

  
    glColor3f( 0.9,  0.5, 0.2 );
   
    glBegin(GL_POLYGON);
    glVertex3f( 0.5, 0.5,0.5 ); //3
    glVertex3f( 0.25, 0.35,0.75 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();

  


    
/*
     glBegin(GL_POLYGON);
    glVertex3f( 0.25, 0.35,0.25 ); //3
    glVertex3f( -0.25, 0.35,0.25 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();
  
    glBegin(GL_POLYGON);
    glVertex3f( -0.5, 0.5,0.5 ); //3
    glVertex3f( -0.25, 0.35,0.25 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();

      glBegin(GL_POLYGON);
    glVertex3f( 0.5, 0.5,0.5 ); //3
    glVertex3f( 0.25, 0.35,0.25 ); //3
    glVertex3f( 0.0, 1.2,0.50 ); //3
    glEnd();
    */

}


void Transformer::drawcylinder(){

float height=0.3;
float PI = 180;
float resolution = 2;
float radius = 0.3;
float i=0.0;
    /* top triangle */
glRotatef(85,1,0,0);
glColor3f(1.0,0,0);
glBegin(GL_TRIANGLE_FAN);
    glVertex3f(0, height, 0);  /* center */
    for (i = 0; i <= 2 * PI; i += resolution)
        glVertex3f(radius * cos(i), height, radius * sin(i));
glEnd();
/* bottom triangle: note: for is in reverse order */
glColor3f(1.0,0,0);

glBegin(GL_TRIANGLE_FAN);
    glVertex3f(0, 0, 0);  /* center */
    for (i = 2 * PI; i >= 0; i -= resolution)
        glVertex3f(radius * cos(i), 0, radius * sin(i));
    /* close the loop back to 0 degrees */
    glVertex3f(radius, height, 0);
glEnd();

/* middle tube */
glColor3f(0.59,0.59,0.59);

glBegin(GL_QUAD_STRIP);
    for (i = 0; i <= 2 * PI; i += resolution)
    {
        glVertex3f(radius * cos(i), 0, radius * sin(i));
        glVertex3f(radius * cos(i), height, radius * sin(i));
    }
    /* close the loop back to zero degrees */
    glVertex3f(radius, 0, 0);
    glVertex3f(radius, height, 0);
glEnd();


}

void Transformer::drawback(){

  // back starts here

    glColor3f( 1.0,  0.0, 0.0 );
    glBegin(GL_POLYGON);
    glVertex3f( 0.25, 0.0, 0.3 );
    glVertex3f( -0.25, 0.0,0.3 );
    glVertex3f( -0.25, -0.35,0.3 );
    glVertex3f( 0.25, -0.35, 0.3 );
    glEnd();

    glColor3f( 0.0,  0.0, 1 );
    glBegin(GL_POLYGON);
    glVertex3f( -0.25, -0.35,0.3 );
    glVertex3f( 0.25, -0.35, 0.3 );
    glVertex3f(  0.5, -0.5, 0.5 );
    glVertex3f( -0.5, -0.5, 0.5 );
    glEnd();
   
     glColor3f(0,  0, 0.9);
     glBegin(GL_POLYGON);
    glVertex3f( -0.5,  0.5, 0.5 );//1
    glVertex3f( -0.25, 0.0,0.3 ); //2
    glVertex3f( -0.25, -0.35,0.3 ); //3
    glVertex3f( -0.5,  -0.5, 0.5 );//1
    glEnd();

     
     //glColor3f( 0.888,  0.888, 0.888 );
    glBegin(GL_POLYGON);
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( 0.25, 0.0,0.3 ); //2
    glVertex3f( 0.25, -0.35,0.3 ); //3
    glVertex3f( 0.5,  -0.5, 0.5 );//1
    glEnd();

   
    glColor3f(0,  0.95, 0.0);
    glBegin(GL_POLYGON);
    glVertex3f( 0.25, 0.0,0.3 ); //2
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( -0.5,  0.5, 0.5);//1
    glVertex3f( -0.25, 0.0,0.3 ); //2
    glEnd();

     glColor3f(0,  1, 0.0);
    glBegin(GL_POLYGON);
    glVertex3f( 0.5,  0.5, 0.5 );//1
    glVertex3f( 0.0,  1.0, 0.5 );//1
    glVertex3f( -0.5,  0.5, 0.5);//1
    glEnd();

}


void Transformer::makechestframe(){
  
  glDepthRange(0.0f, 1.0f);
  glPushMatrix();

    glScalef(1,1.05,1);
    drawback();
    
  glPopMatrix();
  glPushMatrix();

  glScalef(1,1.05,1);
    drawchest();
  glPopMatrix();
  
}

void Transformer::makechest(){

 
 glPushMatrix();
 glTranslatef(0,0.2,0);
  glRotatef(180,0,0,1);
   glPushMatrix();
      glRotatef(90,0,0,1);
      makebone();
    glPopMatrix();
    

   glPushMatrix();
      glTranslatef(0.45,-0.5,0);
      makebone();
   glPopMatrix();

   glPushMatrix();
    
      glTranslatef(0.898,0,0);
      glRotatef(90,0,0,1);
      makebone();
   glPopMatrix();
   glPushMatrix();
    
      glScalef(0.5,1,1);
      glTranslatef(0.45,0.7,0);
      glRotatef(30,0,0,1);
      
      makebone();
   glPopMatrix();
   glPushMatrix();
    
      glScalef(0.5,1,1);
      glTranslatef(1.4,0.7,0);
      glRotatef(-30,0,0,1);
      
      makebone();
   glPopMatrix();

   glPushMatrix();
    
      
      glTranslatef(0.45,0.95,0);
      glRotatef(90,0,0,1);
      glScalef(0.2,1,1);
      makebone();
   glPopMatrix();

   glPushMatrix();
    glTranslatef(0.45,-0.03,-0.5);

    makechestframe();
   glPopMatrix();
glPopMatrix();

  
  


}

void Transformer::makeforeleg_cover(){
  glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
    glColor3f(   0.888,  0.888, 0.888 );
    glBegin(GL_POLYGON);
    glVertex3f(  -0.3, -1, 0.0 );
    glVertex3f(  -0.3, -1, 0.2 );
    glVertex3f(  -0.3, -0.97, 0.2 );
    glVertex3f(  -0.3, -0.93, 0.17 );
    glVertex3f(  -0.3, -0.4, 0.17 );
    glVertex3f(  -0.3, -0.33, 0.27 );
    glVertex3f(  -0.3, -0.2, 0.10 );
    glVertex3f(  -0.3, -0.0, 0.10 );
    glVertex3f(  -0.3, -0.0, -0.0 );
     glVertex3f(  -0.3, -0.28, 0.0 );
     glVertex3f(  -0.3, -1, 0.0 );
    glEnd();

     glColor3f(   0.788,  0.788, 0.788 );
    glBegin(GL_POLYGON);
    glVertex3f(  0.3, -1, 0.0 );
    glVertex3f(  0.3, -1, 0.2 );
    glVertex3f(  0.3, -0.97, 0.2 );
    glVertex3f(  0.3, -0.93, 0.17 );
    glVertex3f(  0.3, -0.4, 0.17 );
    glVertex3f(  0.3, -0.33, 0.27 );
    glVertex3f(  0.3, -0.2, 0.10 );
    glVertex3f(  0.3, -0.0, 0.10 );
    glVertex3f(  0.3, -0.0, -0.0 );
     glVertex3f(  0.3, -0.28, 0.0 );
     glVertex3f(  0.3, -1, 0.0 );
    glEnd();
     glColor3f(   0.688,  0.788, 0.788 );
    glBegin(GL_POLYGON);
      glVertex3f(  0.3, -1, 0.0 );
      glVertex3f(  0.3, 0, 0.0 );
      glVertex3f(  -0.3, 0, 0.0 );
      glVertex3f(  -0.3, -1, 0.0 );
      
    glEnd();
    
      
      
    glEnd();
     glColor3f(   0.688,  0.788, 0.888 );
     glBegin(GL_POLYGON);
      glVertex3f(  0.3, -0.57, 0.17 );
      glVertex3f(  0.3, -0.33, 0.27 );
      glVertex3f(  -0.3, -0.33, 0.27 );
     glVertex3f(  -0.3, -0.57, 0.17 );
      
      
    glEnd();

    glColor3f(   0.788,  0.888, 0.688 );
    glBegin(GL_POLYGON);
    glVertex3f(  -0.3, -0.33, 0.27 );
    glVertex3f(  -0.3, -0.2, 0.10 );
    glVertex3f(  0.3, -0.2, 0.10 );
    glVertex3f(  0.3, -0.33, 0.27 );
    
      
      
    glEnd();
  
}

void Transformer::makethigh_cover(){
glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
    glPushMatrix();
        glPushMatrix();
         glScalef(0.3,0.6,0.05);
         glCallList(structurecolorlist);
        glPopMatrix();

         glPushMatrix();
          glTranslatef(0.0,0.0,-0.2);
           glScalef(0.3,0.6,0.05);
           glCallList(structurecolorlist);
         glPopMatrix();

        glPushMatrix();
          glTranslatef(-0.0,-0.30,-0.1);
          glScalef(0.08,0.05,0.15);
          glCallList(structurecolorlist);
        glPopMatrix();

    glPopMatrix();
     

    
  
}

void Transformer::makeforeleg_backcover(){

  glEnable(GL_DEPTH_TEST);
  glDepthMask(GL_TRUE);
  glDepthFunc(GL_LEQUAL);
  glDepthRange(0.0f, 1.0f);

  glPushMatrix();

        glPushMatrix();
         glScalef(0.3,0.6,0.05);
         glCallList(structurecolorlist);
        glPopMatrix();

  glPopMatrix();


}

void Transformer::makeboot(){
    glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
    glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.0, 0.0 );//
      glVertex3f(  0.0, -0.05, 0.0 );//
      glVertex3f(  0.0, -0.05, 0.1 );//
      glVertex3f(  0.0, -0.2, 0.1 );//
      glVertex3f(  0.0, -0.2, 1 );//
      glVertex3f(  0.0, -0.18, 1 );//
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.0, -0.1, 0.5 );//
      glVertex3f(  0.0, -0.15, 0.5 );//
      glVertex3f(  0.0, -0.15, 0.3 );//
      glVertex3f(  0.0, -0.05, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.0 );
    glEnd();


    glBegin(GL_POLYGON);
      glVertex3f(  0.25, 0.0, 0.0 );//
      glVertex3f(  0.25, -0.05, 0.0 );//
      glVertex3f(  0.25, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.2, 0.1 );//
      glVertex3f(  0.25, -0.2, 1 );//
      glVertex3f(  0.25, -0.18, 1 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.3 );//
      glVertex3f(  0.25, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.00, 0.1 );//
      glVertex3f(  0.25, -0.00, 0.0 );
    glEnd();


    glBegin(GL_POLYGON);
      
      glVertex3f(  0.25, -0.00, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.1 );//
      glVertex3f(  0.0, -0.00, 0.0 );
      glVertex3f(  0.25, -0.00, 0.0 );

    glEnd();
    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.05, 0.1 );//
      glVertex3f(  0.25, -0.00, 0.1 );
      glVertex3f(  0.0, -0.00, 0.1 );


    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.15, 0.3 );//
      glVertex3f(  0.25, -0.15, 0.3 );//
      glVertex3f(  0.25, -0.05, 0.1 );
      glVertex3f(  0.0, -0.05, 0.1 );


    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.15, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.3 );
      glVertex3f(  0.0, -0.15, 0.3 );
      

    glEnd();

     glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, -0.1, 0.5 );//
      glVertex3f(  0.25, -0.1, 0.5 );//
      glVertex3f(  0.25, -0.15, 0.5 );
      glVertex3f(  0.0, -0.15, 0.5 );

    glEnd();

    glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.5 );
      glVertex3f(  0.0, -0.1, 0.5 );

    glEnd();

     glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );//
      glVertex3f(  0.25, -0.1, 0.95 );
      glVertex3f(  0.0, -0.1, 0.95 );

    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.18, 1 );//
      glVertex3f(  0.25, -0.18, 1 );//
      glVertex3f(  0.25, -0.1, 0.95 );
      glVertex3f(  0.0, -0.1, 0.95 );

    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.2, 1 );//
      glVertex3f(  0.25, -0.2, 1 );//
      glVertex3f(  0.25, -0.18, 1 );
      glVertex3f(  0.0, -0.18, 1 );

    glEnd();
    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.2, 0.1 );//
      glVertex3f(  0.25, -0.2, 0.1 );//
      glVertex3f(  0.25, -0.2, 1 );
      glVertex3f(  0.0, -0.2, 1 );

    glEnd();
    
    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.05, 0.1 );
      glVertex3f(  0.25, -0.05, 0.1 );
      glVertex3f(  0.25, -0.2, 0.1 );
      glVertex3f(  0.0, -0.2, 0.1 );

    glEnd();

    glBegin(GL_POLYGON);
      glVertex3f(  0.0, -0.05, 0.0 );
      glVertex3f(  0.25, -0.05, 0.0 );
      glVertex3f(  0.25, -0.05, 0.1 );
      glVertex3f(  0.0, -0.05, 0.1 );

    glEnd();


    glBegin(GL_POLYGON);
       glVertex3f(  0.0, 0.0, 0.0 );
       glVertex3f(  0.25, 0.0, 0.0 );
       glVertex3f(  0.25, -0.05, 0.0 );
       glVertex3f(  0.0, -0.05, 0.0 );

    glEnd();
     

    
  
}

void Transformer::makehindarm_cover(){
  glColor3f(1.0,0,0);
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.0);//
    glVertex3f(0.0,0.0,0.4);//
    glVertex3f(0.0,0.6,0.4);//
    glVertex3f(0.0,0.6,0.3);//
    glVertex3f(0.0,0.3,0.3);//
    glVertex3f(0.0,0.3,0.2);//
    glVertex3f(0.0,0.6,0.2);//
    glVertex3f(0.0,0.6,0.1);//
    glVertex3f(0.0,0.0,0.0);//
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.5,0.0,0.0);//
    glVertex3f(0.5,0.0,0.4);//
    glVertex3f(0.5,0.6,0.4);//
    glVertex3f(0.5,0.6,0.3);//
    glVertex3f(0.5,0.3,0.3);//
    glVertex3f(0.5,0.3,0.2);//
    glVertex3f(0.5,0.6,0.2);//
    glVertex3f(0.5,0.6,0.1);//
    glVertex3f(0.5,0.0,0.0);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.1);//
    glVertex3f(0.5,0.6,0.1);//
    glVertex3f(0.5,0.0,0.0);
    glVertex3f(0.0,0.0,0.0);
  glEnd();


  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.2);//
    glVertex3f(0.5,0.6,0.2);//
    glVertex3f(0.5,0.6,0.1);
    glVertex3f(0.0,0.6,0.1);
   glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.3,0.2);//
    glVertex3f(0.5,0.3,0.2);//
    glVertex3f(0.5,0.6,0.2);
    glVertex3f(0.0,0.6,0.2);

  glEnd();
  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.3,0.3);//
    glVertex3f(0.5,0.3,0.3);//
    glVertex3f(0.5,0.3,0.2);
    glVertex3f(0.0,0.3,0.2);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.3);//
    glVertex3f(0.5,0.6,0.3);//
    glVertex3f(0.5,0.3,0.3);
    glVertex3f(0.0,0.3,0.3);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.6,0.4);//
    glVertex3f(0.5,0.6,0.4);//
    glVertex3f(0.5,0.6,0.3);
    glVertex3f(0.0,0.6,0.3);
  glEnd();


  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.0);
    glVertex3f(0.5,0.0,0.0);
    glVertex3f(0.5,0.0,0.4);
    glVertex3f(0.0,0.0,0.4);
  glEnd();

  glBegin(GL_POLYGON);
    glVertex3f(0.0,0.0,0.4);//
    glVertex3f(0.5,0.0,0.4);//
    glVertex3f(0.5,0.6,0.4);
    glVertex3f(0.0,0.6,0.4);
  glEnd();

}

void Transformer::makeleftshoulder(){
  glPushMatrix();
    glPushMatrix();
      glScalef(0.2,1,1);
      makebone();
    glPopMatrix();

    
  glPopMatrix();
}

void Transformer::makerightshoulder(){
  glPushMatrix();
    glPushMatrix();
        glRotatef(180,0,0,1);
        glScalef(0.2,1,1);
        makebone();
    glPopMatrix();
    
  glPopMatrix();
}

void Transformer::makelefthindarm(){
  glPushMatrix();
    glPushMatrix();
        glRotatef(-90,0,0,1);
        glScalef(0.7,1,1);
        makebone();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0.13,-0.2,0.1);
      glScalef(0.7,1.0,0.5);
      glRotatef(180,0,1,0);
     makehindarm_cover();
    glPopMatrix();

    glPushMatrix();
      glTranslatef(-0.55,0.1,0);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
     drawcylinder();
    glPopMatrix();

  glPopMatrix();
}

void Transformer::makeleftforearm(){
  glPushMatrix();
    glPushMatrix();
          glRotatef(-90,0,0,1);
          glScalef(0.7,1,1);
          makebone();
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-0.2,-0.23,0);
      glScalef(1,1.5,0.5);
     makeforearm_cover();
    glPopMatrix();

    glPushMatrix();
    glTranslatef(-0.2,-0.23,-0.1);
      glScalef(1,1.5,0.5);
     makeforearm_cover();
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makerightforearm(){
  glPushMatrix();
    glPushMatrix();
          glRotatef(-90,0,0,1);
          glScalef(0.7,1,1);
          makebone();
    glPopMatrix();
    glPushMatrix();
    glTranslatef(-0.2,-0.23,0);
      glScalef(1,1.5,0.5);
      makeforearm_cover();
    glPopMatrix();

     glPushMatrix();
    glTranslatef(-0.2,-0.23,-0.1);
      glScalef(1,1.5,0.5);
     makeforearm_cover();
    glPopMatrix();


  glPopMatrix();
}


void Transformer::makerighthindarm(){
  glPushMatrix();
    glPushMatrix();
        glRotatef(-90,0,0,1);
        glScalef(0.7,1,1);
        makebone();
    glPopMatrix();
    glPushMatrix();
     glTranslatef(0.22,-0.2,0.1);
      glScalef(0.7,1.0,0.5);
      glRotatef(180,0,1,0);
      makehindarm_cover();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0.24,0.15,0);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
     drawcylinder();
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makeneck(){

      glPushMatrix();
        glRotatef(90,0,0,1);
        glScalef(0.3,1,1);
        makebone();
      glPopMatrix();


}

void Transformer::makehead(){


      glPushMatrix();
        glTranslatef(0,0.15,0);
        glScalef(0.3,0.3,0.3);
        glCallList(structurelist);
      glPopMatrix();



}

void Transformer::makeleftbuttuck(){
  glPushMatrix();
    
    glScalef(0.5,1.0,1.0);
    makebone();

  glPopMatrix();
  glPushMatrix();
        glTranslatef(-0.20,-0.10,0);
        glRotatef(90,0,0,1);
        glScalef(0.10,1.0, 1.0);
        makebone();
  glPopMatrix();


}

void Transformer::makerightbuttuck(){
  glPushMatrix();
    
    glScalef(0.5,1.0,1.0);
    makebone();

  glPopMatrix();
  glPushMatrix();
        glTranslatef(0.20,-0.10,0);
        glRotatef(90,0,0,1);
        glScalef(0.10,1.0, 1.0);
        makebone();
  glPopMatrix();


}

void Transformer::makeleftleg(){
  glPushMatrix();
    glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1,1.0);
       makebone();
    glPopMatrix();

    glPushMatrix();
      glTranslatef(0,0.15,0.1);
      glScalef(1.9,1,1);
     makethigh_cover();
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makerightleg(){
  glPushMatrix();
    glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1.0,1.0);
       makebone();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0,0.15,0.1);
      glScalef(1.9,1,1);
     makethigh_cover();
    glPopMatrix();
  glPopMatrix();
}

void Transformer::makeleftforeleg(){
  glPushMatrix();
  	 glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1,1.0);
       makebone();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0,0.3,0.03);
      glScalef(1,0.7,1.0);
     makeforeleg_cover();
    glPopMatrix();

    glPushMatrix();
      glTranslatef(-0.6,0,0.1);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
     drawcylinder();
    glPopMatrix();

    glPushMatrix();
      glTranslatef(0,-0.05,-0.05);
      glScalef(1.8,1,1);
     makeforeleg_backcover();
    glPopMatrix();
    
  glPopMatrix();

}
void Transformer::makerightforeleg(){
  glPushMatrix();
  	 glPushMatrix();
       glRotatef(-90,0,0,1);
       glScalef(0.5,1,1.0);
       makebone();
    glPopMatrix();
     glPushMatrix();
      glTranslatef(0,0.3,0.03);
      glScalef(1,0.7,1.0);
     makeforeleg_cover();
    glPopMatrix();
    glPushMatrix();
      glTranslatef(0.34,0,0.1);
      glScalef(1,1,0.5);
      glRotatef(90,0,1,0);
     drawcylinder();
    glPopMatrix();
     glPushMatrix();
      glTranslatef(0,-0.05,-0.05);
      glScalef(1.8,1,1);
     makeforeleg_backcover();
    glPopMatrix();
  glPopMatrix();
}



void Transformer::makearmweapon(){

  glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
    
    glColor3f(0.888,0.888,0.888);
     glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.0, 0.0 );
      glVertex3f(  0.0, -0.3, 0.2 );
      glVertex3f(0.0,0.0,1.0);
    glEnd();

      glColor3f(0.888,0.888,0.788);
     glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.05, 0.0 );
      glVertex3f(  0.0, 0.35, 0.2 );
      glVertex3f(0.0,0.05,1.0);
    glEnd();
     glColor3f(0.888,0.788,0.888);
     glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.1, -0.2 );
      glVertex3f(  0.0, -0.1, -0.2 );
      glVertex3f(0.0,0.025,0.2);
    glEnd();
     glColor3f(0.788,0.888,0.888);
     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.0, 0.0 );
      glVertex3f(  0.05, -0.3, 0.2 );
      glVertex3f(0.05,0.0,1.0);
    glEnd();

     glColor3f(0.688,0.888,0.788);
     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.05, 0.0 );
      glVertex3f(  0.05, 0.35, 0.2 );
      glVertex3f(0.05,0.05,1.0);
    glEnd();
     glColor3f(0.888,0.688,0.788);
     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.1, -0.2 );
      glVertex3f(  0.05, -0.1, -0.2 );
      glVertex3f(0.05,0.025,0.2);
    glEnd();


  glBegin(GL_POLYGON);
      
      glVertex3f(  0.0, 0.0, 0.0 );
      glVertex3f(  0.05, 0.0, 0.0 );
      glVertex3f(  0.05, -0.3, 0.2 );
      glVertex3f(  0.00, -0.3, 0.2 );
  glEnd();

     glBegin(GL_POLYGON);
      glVertex3f(  0.05, -0.3, 0.2 );
      glVertex3f(  0.00, -0.3, 0.2 );
      glVertex3f(  0.00, 0.0, 1 );
      glVertex3f(  0.05, 0.0, 1 );
    glEnd();

     glBegin(GL_POLYGON);  
      glVertex3f(  0.0, 0.05, 0.0 );
      glVertex3f(  0.05, 0.05, 0.0 );
      glVertex3f(  0.05, 0.35, 0.2 );
      glVertex3f(  0.00, 0.35, 0.2 );
    glEnd();

     glBegin(GL_POLYGON);
      glVertex3f(  0.05, 0.35, 0.2 );
      glVertex3f(  0.00, 0.35, 0.2 );
      glVertex3f(  0.00, 0.05, 1 );
      glVertex3f(  0.05, 0.05, 1 );
    glEnd();


    glBegin(GL_POLYGON);
      glVertex3f(  0.0, 0.1, -0.2 );
      glVertex3f(  0.05, 0.1, -0.2 );
      glVertex3f(  0.05, -0.1, -0.2 );
      glVertex3f(  0.0, -0.1, -0.2 );

    glEnd();

    glBegin(GL_POLYGON);
     glVertex3f(  0.0, -0.1, -0.2 );
     glVertex3f(  0.05, -0.1, -0.2 );
     glVertex3f(0.05,0.025,0.2);
     glVertex3f(0.0,0.025,0.2);

    glEnd();

    glBegin(GL_POLYGON);
     glVertex3f(0.0,0.025,0.2);
     glVertex3f(0.05,0.025,0.2);
     glVertex3f(  0.05, 0.1, -0.2 );
     glVertex3f(  0.0, 0.1, -0.2 );

    glEnd();

    glTranslatef(-0.01,0.1,-0.26);
    glScalef(0.15,0.3,0.15);
    glCallList(structurecolorlist);




     

    
  
}


void Transformer::makemodel(){
	
	glEnable(GL_DEPTH_TEST);
glDepthMask(GL_TRUE);
glDepthFunc(GL_LEQUAL);
glDepthRange(0.0f, 1.0f);
	glPushMatrix();
    glScalef(modelscale_x,modelscale_y,modelscale_z);
    glPushMatrix();
      
      glTranslatef(-0.45,-0.8,0);
      glRotatef(rotatechest_x,1,0,0);
      glRotatef(rotatechest_y,0,1,0);
      glRotatef(rotatechest_z,0,0,1);
      glTranslatef(0.45,0.8,0);
      makechest();

      glPushMatrix();
        glTranslatef(-0.43,0.86,0);
        glRotatef(rotateneck_x,1,0,0);
        glRotatef(rotateneck_y,0,1,0);
        glRotatef(rotateneck_z,0,0,1);
        makeneck();
        glPushMatrix();
          glScalef(1,1,0.5);
          makehead();
        glPopMatrix();

      glPopMatrix();


      glPushMatrix();
        glTranslatef(-1,0.7,0);
        makeleftshoulder();
        glPushMatrix();
          glTranslatef(-0.1,-0.3,0);
          glTranslatef(0.0,0.25,0);
          glRotatef(rotatelefthindarm_x,1,0,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotatelefthindarm_y,0,1,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotatelefthindarm_z,0,0,1);
          glTranslatef(0.0,-0.25,0);


          makelefthindarm();
          glPushMatrix();

            glTranslatef(0.0,-0.7,0);
            glRotatef(0,0,0,1);

            glTranslatef(0.0,0.34,0);
            glRotatef(-10+rotateleftforearm,1,0,0);
            glTranslatef(0.0,-0.34,0);  

            makeleftforearm();

            glPushMatrix();
              glTranslatef(0,-0.5,0.25);
              glScalef(1,1,0.8);
              makearmweapon();
            glPopMatrix();
          glPopMatrix();

        glPopMatrix();

      glPopMatrix();




      glPushMatrix();
        glTranslatef(0.1,0.7,0);
        makerightshoulder();
        glPushMatrix();
          glTranslatef(0.1,-0.3,0);


          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindarm_x,1,0,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindarm_y,0,1,0);
          glTranslatef(0.0,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindarm_z,0,0,1);
          glTranslatef(0.0,-0.25,0);
          makerighthindarm();


          glPushMatrix();

            glTranslatef(0.0,-0.7,0);
            glRotatef(0,0,0,1);
            glTranslatef(0.0,0.34,0);
            glRotatef(rotaterightforearm,1,0,0);
            glTranslatef(0.0,-0.34,0);  
            makerightforearm();
               glPushMatrix();
                glTranslatef(0,-0.5,0.25);
                glScalef(1,1,0.8);
                makearmweapon();
              glPopMatrix();
          glPopMatrix();

        glPopMatrix();

      glPopMatrix();
    
      glPopMatrix();

    glPushMatrix();
        glTranslatef(-0.20,-0.82,0);
        makerightbuttuck();
        glPushMatrix();
          glTranslatef(0.2,-0.4,0);

          glTranslatef(0.2,0.25,0);
          glRotatef(rotaterighthindleg_x,1,0,0);
          glTranslatef(-0.2,-0.25,0);

          glTranslatef(0.0,0.25,0);
          glRotatef(rotaterighthindleg_z,0,0,1);
          glTranslatef(0.0,-0.25,0);

          makerightleg();
          glPushMatrix();
            glTranslatef(0.0,-0.5,0);
            glTranslatef(-0.91,0.25,0);
            glRotatef(rotaterightforeleg,1,0,0);
            glTranslatef(0.91,-0.25,0);
            makerightforeleg();

            glPushMatrix();
              glScalef(2.4,1.5,0.8);
              glTranslatef(-0.12,-0.2,-0.3);
              makeboot();
            glPopMatrix();
          glPopMatrix();
        glPopMatrix();


      glPopMatrix();


  glPushMatrix();
        glTranslatef(-0.70,-0.82,0);
        makeleftbuttuck();
        glPushMatrix();
          glTranslatef(-0.2,-0.4,0);
          glTranslatef(-0.2,0.25,0);
          glRotatef(rotatelefthindleg_x,1,0,0);
          glTranslatef(0.2,-0.25,0);

          glTranslatef(-0.0,0.25,0);
          
          glRotatef(rotatelefthindleg_z,0,0,1);
          
          glTranslatef(0.0,-0.25,0);

          makeleftleg();
          glPushMatrix();
            glTranslatef(0.0,-0.5,0);
            glTranslatef(0.91,0.25,0);
            glRotatef(rotateleftforeleg,1,0,0);
            glTranslatef(-0.91,-0.25,0);
            makeleftforeleg();
            glPushMatrix();
              glScalef(2.4,1.5,0.8);
              glTranslatef(-0.12,-0.2,-0.3);
              makeboot();
            glPopMatrix();
          glPopMatrix();
        glPopMatrix();
        


      glPopMatrix();

      


  glPopMatrix();

}

void renderGL( void )
{
  // intialize transformer object
Transformer t1;
t1.init();
glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
glClearDepth(1.0f);
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


glLoadIdentity();
glTranslatef(0,0.3,0);
glRotatef( rotate_x, 1.0, 0.0, 0.0 );
glRotatef( rotate_y, 0.0, 1.0, 0.0 );

// make model
t1.makemodel();

glFlush();

}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    //!Close the window if the ESC key was pressed
    if (key == GLFW_KEY_ESCAPE  && action == GLFW_PRESS)
      glfwSetWindowShouldClose(window, GL_TRUE);
    if (key == GLFW_KEY_RIGHT && keyctrl==none && action == GLFW_PRESS){
      rotate_y += 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_LEFT && keyctrl==none && action == GLFW_PRESS){
      rotate_y -= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_UP && keyctrl==none && action == GLFW_PRESS){
      rotate_x += 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_DOWN && keyctrl==none && action == GLFW_PRESS){
      rotate_x -= 5;
      glfwSwapBuffers(window);
    }


    if(key == GLFW_KEY_0 && action == GLFW_PRESS){
        keyctrl=none;
    }

    if(key == GLFW_KEY_Y && action == GLFW_PRESS){
        keyctrl=neck;
    }

    if(key == GLFW_KEY_G && action == GLFW_PRESS){
        keyctrl=left_hind_arm;
    }

    if(key == GLFW_KEY_H && action == GLFW_PRESS){
        keyctrl=chest;
    }
    if(key == GLFW_KEY_V && action == GLFW_PRESS){
        keyctrl=left_fore_arm;
    }
    if(key == GLFW_KEY_J && action == GLFW_PRESS){
        keyctrl=right_hind_arm;
    }
    if(key == GLFW_KEY_M && action == GLFW_PRESS){
        keyctrl=right_fore_arm;
    }
    if(key == GLFW_KEY_I && action == GLFW_PRESS){
        keyctrl=left_hind_leg;
    }
    if(key == GLFW_KEY_K && action == GLFW_PRESS){
        keyctrl=left_fore_leg;
    }
    if(key == GLFW_KEY_O && action == GLFW_PRESS){
        keyctrl=right_hind_leg;
    }
     if(key == GLFW_KEY_L && action == GLFW_PRESS){
        keyctrl=right_fore_leg;
    }

    if(key == GLFW_KEY_N && action == GLFW_PRESS){
        keyctrl=right_boot;
    }


    if (key == GLFW_KEY_Q && keyctrl==neck && action == GLFW_PRESS){
      if(rotateneck_z>=-15)
        rotateneck_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==neck && action == GLFW_PRESS){
      if(rotateneck_z<=15)
        rotateneck_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A && keyctrl==neck && action == GLFW_PRESS){
      if(rotateneck_x>=-30)
        rotateneck_x-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D && keyctrl==neck && action == GLFW_PRESS){
      if(rotateneck_x<=30)
        rotateneck_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_A && keyctrl==left_fore_leg && action == GLFW_PRESS){
      if(rotateleftforeleg<=30)
        	rotateleftforeleg+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D &&  keyctrl==left_fore_leg && action == GLFW_PRESS){
      if(rotateleftforeleg>=-30)
        	rotateleftforeleg-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_A && keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(rotatelefthindleg_x<=60)
        	rotatelefthindleg_x+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D &&  keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(rotatelefthindleg_x>=-60)
        	rotatelefthindleg_x-= 5;
      glfwSwapBuffers(window);
    }
     if (key == GLFW_KEY_E &&  keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(rotatelefthindleg_z<=30)
        	rotatelefthindleg_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_Q &&  keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(rotatelefthindleg_z>=-30)
        	rotatelefthindleg_z-= 5;
      glfwSwapBuffers(window);
    }

//right leg start from here

    if (key == GLFW_KEY_D &&  keyctrl==right_fore_leg && action == GLFW_PRESS){
      if(rotaterightforeleg<=30)
          rotaterightforeleg+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A && keyctrl==right_fore_leg && action == GLFW_PRESS){
      if(rotaterightforeleg>=-30)
          rotaterightforeleg-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D &&  keyctrl==right_hind_leg && action == GLFW_PRESS){
      if(rotaterighthindleg_x<=60)
          rotaterighthindleg_x+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A  &&  keyctrl==right_hind_leg  && action == GLFW_PRESS){
      if(rotaterighthindleg_x>=-60)
          rotaterighthindleg_x-= 5;
      glfwSwapBuffers(window);
    }
     if (key == GLFW_KEY_E  &&  keyctrl==right_hind_leg  && action == GLFW_PRESS){
      if(rotaterighthindleg_z<=30)
          rotaterighthindleg_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_Q  &&  keyctrl==right_hind_leg  && action == GLFW_PRESS){
      if(rotaterighthindleg_z>=-30)
          rotaterighthindleg_z-= 5;
      glfwSwapBuffers(window);
    }

    //chest start from here
    if (key == GLFW_KEY_W &&  keyctrl==chest && action == GLFW_PRESS){
      if(rotatechest_y<=175)
          rotatechest_y+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_S && keyctrl==chest && action == GLFW_PRESS){
      if(rotatechest_y>=-175)
          rotatechest_y-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==chest && action == GLFW_PRESS){
      if(rotatechest_x<=90)
          rotatechest_x+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A && keyctrl==chest && action == GLFW_PRESS){
      if(rotatechest_x>=-40)
          rotatechest_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_E && keyctrl==chest && action == GLFW_PRESS){
      if(rotatechest_z<=30)
          rotatechest_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_Q && keyctrl==chest && action == GLFW_PRESS){
      if(rotatechest_z>=-30)
          rotatechest_z-= 5;
      glfwSwapBuffers(window);
    }


    //making the arms


     if (key == GLFW_KEY_A && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(rotatelefthindarm_x>=-30)
          rotatelefthindarm_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(rotatelefthindarm_x<=30)
          rotatelefthindarm_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_S && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(rotatelefthindarm_y>=-30)
          rotatelefthindarm_y-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_W && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(rotatelefthindarm_y<=30)
          rotatelefthindarm_y+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_Q && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(rotatelefthindarm_z>=-30)
          rotatelefthindarm_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(rotatelefthindarm_z<=30)
          rotatelefthindarm_z+= 5;
      glfwSwapBuffers(window);
    }


     if (key == GLFW_KEY_A && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(rotaterighthindarm_x>=-30)
          rotaterighthindarm_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(rotaterighthindarm_x<=30)
          rotaterighthindarm_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_S && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(rotatelefthindarm_y>=-30)
          rotatelefthindarm_y-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_W && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(rotaterighthindarm_y<=30)
          rotaterighthindarm_y+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_Q && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(rotaterighthindarm_z>=-30)
          rotaterighthindarm_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(rotaterighthindarm_z<=30)
          rotaterighthindarm_z+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_A && keyctrl==left_fore_arm && action == GLFW_PRESS){
      if(rotateleftforearm>=-140)
          rotateleftforearm-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D && keyctrl==left_fore_arm && action == GLFW_PRESS){
      if(rotateleftforearm<=0)
          rotateleftforearm+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_A && keyctrl==left_fore_arm && action == GLFW_PRESS){
      if(rotaterightforearm>=-140)
          rotaterightforearm-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==left_fore_arm && action == GLFW_PRESS){
      if(rotaterightforearm<=0)
          rotaterightforearm+= 5;
      glfwSwapBuffers(window);
    }
   


  }

int main (int argc, char *argv[]) 
{
  //progname=argv[0];

  //! The pointer to the GLFW window
  GLFWwindow* window;
  //glutInit(&argc,argv);

  //! Setting up the GLFW Error callback
  glfwSetErrorCallback(csX75::error_callback);

  //! Initialize GLFW
  if (!glfwInit())
    return -1;

  //setup(argc, argv);

  int win_width=512;
  int win_height=512;

  //! Create a windowed mode window and its OpenGL context
  window = glfwCreateWindow(win_width, win_height, "Transformers", NULL, NULL);
  if (!window)
    {
      glfwTerminate();
      return -1;
    }
  
  //! Make the window's context current 
  glfwMakeContextCurrent(window);

  //Keyboard Callback
  glfwSetKeyCallback(window, key_callback);
  //Framebuffer resize callback
  glfwSetFramebufferSizeCallback(window, csX75::framebuffer_size_callback);

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

  glfwGetFramebufferSize(window, &win_width, &win_height);
  csX75::framebuffer_size_callback(window, win_width, win_height);
  //Initialize GL state
  csX75::initGL();

  // Loop until the user closes the window
  while (glfwWindowShouldClose(window) == 0)
    {
       
      // Render here
      //usleep(100000);
      renderGL();

      // Swap front and back buffers
      glfwSwapBuffers(window);
      
      // Poll for and process events
      glfwPollEvents();
    }

  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}
