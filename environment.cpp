#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <string>
#include <math.h>

#include <stdio.h>
#include "SOIL/SOIL.h"
#include "environment.hpp"
#define PI 3.14159265

void Environment::drawenvironment(){


   // ----- Render the Color Cube -----
   
       
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, sky);
        glBegin(GL_QUADS); // of the color cube

        // Top-face
        
        
       // glColor3f(0.0f, 1.0f, 0.0f); // green
        glTexCoord2f(0.0, 0.0);glVertex3f(1.0f, 1.0f, -1.0f);
        glTexCoord2f(0.0, 1.0);glVertex3f(-1.0f, 1.0f, -1.0f);
        glTexCoord2f(1.0, 0.0);glVertex3f(-1.0f, 1.0f, 1.0f);
        glTexCoord2f(1.0, 1.0);glVertex3f(1.0f, 1.0f, 1.0f);
        glDisable(GL_TEXTURE_2D);
        glEnd();
        
        // Bottom-face
       
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
         glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, road);
        glBegin(GL_QUADS);
        //glColor3f(1.0f, 0.5f, 0.0f); // orange
        glTexCoord2f(0.0, 0.0);glVertex3f(1.0f, -1.0f, 1.0f);
        glTexCoord2f(0.0, 1.0);glVertex3f(-1.0f, -1.0f, 1.0f);
        glTexCoord2f(1.0, 0.0);glVertex3f(-1.0f, -1.0f, -1.0f);
        glTexCoord2f(1.0, 1.0);glVertex3f(1.0f, -1.0f, -1.0f);
        glDisable(GL_TEXTURE_2D);
        glEnd();
        
        // Front-face
        
         glEnable(GL_TEXTURE_2D);
     glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
     glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
     glBindTexture(GL_TEXTURE_2D, walls);
        glBegin(GL_QUADS);
        //glColor3f(1.0f, 0.0f, 0.0f); // red

        glTexCoord2f(0.0, 0.0);glVertex3f(1.0f, 1.0f, 1.0f);
        glTexCoord2f(0.0, 1.0);glVertex3f(-1.0f, 1.0f, 1.0f);
        glTexCoord2f(1.0, 0.0);glVertex3f(-1.0f, -1.0f, 1.0f);
        glTexCoord2f(1.0, 1.0);glVertex3f(1.0f, -1.0f, 1.0f);
        glDisable(GL_TEXTURE_2D);
        glEnd();
        
        // Back-face
       
        glEnable(GL_TEXTURE_2D);
     glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
      glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
     glBindTexture(GL_TEXTURE_2D, walls);
        glBegin(GL_QUADS);
        //glColor3f(1.0f, 1.0f, 0.0f); // yellow
        glTexCoord2f(0.0, 0.0);glVertex3f(1.0f, -1.0f, -1.0f);
        glTexCoord2f(0.0, 1.0);glVertex3f(-1.0f, -1.0f, -1.0f);
        glTexCoord2f(1.0, 0.0);glVertex3f(-1.0f, 1.0f, -1.0f);
        glTexCoord2f(1.0, 1.0);glVertex3f(1.0f, 1.0f, -1.0f);
        glDisable(GL_TEXTURE_2D);
        glEnd();
        
        // Left-face
       
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
         glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glBindTexture(GL_TEXTURE_2D, walls);
        glBegin(GL_QUADS);
        //glColor3f(0.0f, 0.0f, 1.0f); // blue
        glTexCoord2f(0.0, 0.0);glVertex3f(-1.0f, 1.0f, 1.0f);
        glTexCoord2f(0.0, 1.0);glVertex3f(-1.0f, 1.0f, -1.0f);
        glTexCoord2f(1.0, 0.0);glVertex3f(-1.0f, -1.0f, -1.0f);
        glTexCoord2f(1.0, 1.0);glVertex3f(-1.0f, -1.0f, 1.0f);
        glDisable(GL_TEXTURE_2D);
        glEnd();
        
        // Right-face
        glEnable(GL_TEXTURE_2D);
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

        glBindTexture(GL_TEXTURE_2D, walls);
        glBegin(GL_QUADS);
        
        glTexCoord2f(0.0, 0.0);glVertex3f(1.0f, 1.0f, -1.0f);
        glTexCoord2f(0.0, 1.0);glVertex3f(1.0f, 1.0f, 1.0f);
        glTexCoord2f(1.0, 0.0);glVertex3f(1.0f, -1.0f, 1.0f);
        glTexCoord2f(1.0, 1.0);glVertex3f(1.0f, -1.0f, -1.0f);
        glDisable(GL_TEXTURE_2D);
        glEnd(); // of the color cube
        

}

void Environment::envinit(){

   glClearColor (0.0, 0.0, 0.0, 0.0);
   glShadeModel(GL_FLAT);
   glEnable(GL_DEPTH_TEST);
   glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

   glGenTextures(1, &road);
   glBindTexture(GL_TEXTURE_2D, road);
   unsigned char* image = SOIL_load_image("road.jpg", &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
              GL_UNSIGNED_BYTE, image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  SOIL_free_image_data(image);

  /////////////////////////for sky ///////////////////////////////////////////////////////////////////
  glGenTextures(1, &sky);
   glBindTexture(GL_TEXTURE_2D, sky);
   unsigned char* skyimage = SOIL_load_image("sky.jpg", &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
              GL_UNSIGNED_BYTE, skyimage);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  SOIL_free_image_data(skyimage);

  glGenTextures(1, &walls);
   glBindTexture(GL_TEXTURE_2D, walls);
   unsigned char* wallimage = SOIL_load_image("wall.jpg", &width, &height, 0, SOIL_LOAD_RGB);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
              GL_UNSIGNED_BYTE, wallimage);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  SOIL_free_image_data(wallimage);

  
    glShadeModel(GL_SMOOTH);
   glEnable(GL_DEPTH_TEST);
   glFrontFace(GL_CW);       // clockwise polygons face out
   //glEnable(GL_CULL_FACE);    // Do not try to display the back sides



  //glEnable(GL_COLOR_MATERIAL);
  glEnable(GL_LIGHTING);
 
   glEnable(GL_LIGHT1);
 //  //
  glEnable(GL_LIGHT2);
  

  //drawenvironment();

}
