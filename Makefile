OPENGLLIB= -lGL
GLEWLIB= -lGLEW
GLFWLIB = -lglfw
SOILLIB = -lSOIL
GLUTLIB = -lGLU
SDLLIB = -I/usr/include/SDL -lSDL -std=c++11
LIBS=$(OPENGLLIB) $(GLEWLIB) $(GLFWLIB) $(SOILLIB) $(GLUTLIB) $(SDLLIB)
LDFLAGS=-L/usr/local/lib 
CPPFLAGS=-I/usr/local/include

BIN=transformer
SRCS=gl_framework.cpp transformer.cpp environment.cpp main.cpp 
INCLUDES=gl_framework.hpp transformer.hpp environment.hpp



all: $(BIN)


$(BIN): $(SRCS) $(INCLUDES)
	g++ $(CPPFLAGS) $(SRCS) -o $(BIN) $(LDFLAGS) $(LIBS)

clean:
	rm -f $(BIN)
	rm -f *.o
	rm -f *~


