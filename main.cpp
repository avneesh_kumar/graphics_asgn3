
#include <cstdlib>
#include <unistd.h>
#include <iostream>
#include <string>
#include <math.h>
#include "gl_framework.hpp"
#include "transformer.hpp"
#include "environment.hpp"
#include <stdio.h>
#include <string.h>
#include <vector>
#include <cstdlib>
#include <fstream>
#include <SDL_image.h>
using namespace std;
#define PI 3.14159265


void renderGL();

ofstream myfile;

int numberofframes =5;
int win_width=720;
  int win_height=720;



 Transformer  t1;
 GLFWwindow* window;
 float rotate_x=0.0f;
 float rotate_y=0.0f;
 int trflag=0;
 int camFlag=0;
 int light1flag=1;
 int light2flag=1;
 float movecarforward=0;
 float rotatecar_y=0;
 float dir=0;
 float tx=0;
 float tz=0;
 float lookatx=0;
 float lookatz=1;
 float camcfx=0;
 float camcfz=0.83;
 float camcx=0;
 float camcz=-1.85;
 float cambx = 0; 
 float cambz = -0.98;
 float cambfx = 0 ;
 float cambfz = -0.01;
//GLFW display callback


 void saveframe(){

      myfile <<t1.rotatelefthindarm_x<<",";
      myfile <<t1.rotatelefthindarm_y<<",";
      myfile <<t1.rotatelefthindarm_z<<",";

      /****rotate controller angles for fore arm****/
      myfile <<t1.rotateleftforearm<<",";

      /****rotate controller angles for hind arm****/
      myfile <<t1.rotaterighthindarm_x<<",";
      myfile <<t1.rotaterighthindarm_y<<",";
      myfile <<t1.rotaterighthindarm_z<<",";

      /****rotate controller angles for fore arm****/
      myfile <<t1.rotaterightforearm<<",";

      /****rotate controller angles for hind leg****/
      myfile <<t1.rotatelefthindleg_x<<",";
      myfile <<t1.rotatelefthindleg_y<<",";
      myfile <<t1.rotatelefthindleg_z<<",";

      myfile <<t1.rotateleftarmweapon_x<<",";
      myfile <<t1.rotateleftarmweapon_y<<",";
      myfile <<t1.rotateleftarmweapon_z<<",";

      myfile <<t1.rotaterightarmweapon_x<<",";
      myfile <<t1.rotaterightarmweapon_y<<",";
      myfile <<t1.rotaterightarmweapon_z<<",";

      myfile <<t1.rotateleftboot_x<<",";
      myfile <<t1.rotateleftboot_y<<",";
      myfile <<t1.rotateleftboot_z<<",";

      myfile <<t1.rotaterightboot_x<<",";
      myfile <<t1.rotaterightboot_y<<",";
      myfile <<t1.rotaterightboot_z<<",";

      


      /****rotate controller angles for fore leg****/
      myfile <<t1.rotateleftforeleg<<",";

      /****rotate controller angles for hind leg****/
      myfile <<t1.rotaterighthindleg_x<<",";
      myfile <<t1.rotaterighthindleg_y<<",";
      myfile <<t1.rotaterighthindleg_z<<",";

      /****rotate controller angles for fore leg****/
      myfile <<t1.rotaterightforeleg<<",";

      /****rotate controller angles for  neck****/
      myfile <<t1.rotateneck_x<<",";
      myfile <<t1.rotateneck_y<<",";
      myfile <<t1.rotateneck_z<<",";

      myfile <<t1.rotatechest_x<<",";
      myfile <<t1.rotatechest_y<<",";
      myfile <<t1.rotatechest_z<<",";

      myfile <<t1.bonescale_x<<",";
      myfile <<t1.bonescale_y<<",";
      myfile <<t1.bonescale_z<<",";

      myfile <<t1.modelscale_x<<",";
      myfile <<t1.modelscale_y<<",";
      myfile <<t1.modelscale_z<<",";

      // wheel rotate
      myfile <<t1.rotateLeftfWheel<<",";
      myfile <<t1.rotateLeftbWheel<<",";
      myfile <<t1.rotateRightfWheel<<",";
      myfile <<t1.rotateRightbWheel<<",";

     myfile <<rotate_x<<",";     
     myfile <<rotate_y<<",";     
     myfile <<trflag<<",";
     myfile <<camFlag<<",";
     myfile <<light1flag<<",";
     myfile <<light2flag<<",";
     myfile <<movecarforward<<",";
     myfile <<rotatecar_y<<",";
     myfile <<dir<<",";
     myfile <<tx<<",";
     myfile <<tz<<",";
     myfile <<lookatx<<",";
     myfile <<lookatz<<",";
     myfile <<camcfx<<",";
     myfile <<camcfz<<",";
     myfile <<camcx<<",";
     myfile <<camcz<<",";
     myfile <<cambx<<","; 
     myfile <<cambz<<",";
     myfile <<cambfx<<",";
     myfile <<cambfz;

      myfile<<"\n";
 }

 void saveframe1(){

      ofstream myfile1;
      myfile1.open("st.txt",ios::app);

      myfile1 <<t1.rotatelefthindarm_x<<",";
      myfile1 <<t1.rotatelefthindarm_y<<",";
      myfile1 <<t1.rotatelefthindarm_z<<",";

      /****rotate controller angles for fore arm****/
      myfile1 <<t1.rotateleftforearm<<",";

      /****rotate controller angles for hind arm****/
      myfile1 <<t1.rotaterighthindarm_x<<",";
      myfile1 <<t1.rotaterighthindarm_y<<",";
      myfile1 <<t1.rotaterighthindarm_z<<",";

      /****rotate controller angles for fore arm****/
      myfile1 <<t1.rotaterightforearm<<",";

      /****rotate controller angles for hind leg****/
      myfile1 <<t1.rotatelefthindleg_x<<",";
      myfile1 <<t1.rotatelefthindleg_y<<",";
      myfile1 <<t1.rotatelefthindleg_z<<",";

      myfile1 <<t1.rotateleftarmweapon_x<<",";
      myfile1 <<t1.rotateleftarmweapon_y<<",";
      myfile1 <<t1.rotateleftarmweapon_z<<",";

      myfile1 <<t1.rotaterightarmweapon_x<<",";
      myfile1 <<t1.rotaterightarmweapon_y<<",";
      myfile1 <<t1.rotaterightarmweapon_z<<",";

      myfile1 <<t1.rotateleftboot_x<<",";
      myfile1 <<t1.rotateleftboot_y<<",";
      myfile1 <<t1.rotateleftboot_z<<",";

      myfile1 <<t1.rotaterightboot_x<<",";
      myfile1 <<t1.rotaterightboot_y<<",";
      myfile1 <<t1.rotaterightboot_z<<",";

      


      /****rotate controller angles for fore leg****/
      myfile1 <<t1.rotateleftforeleg<<",";

      /****rotate controller angles for hind leg****/
      myfile1 <<t1.rotaterighthindleg_x<<",";
      myfile1 <<t1.rotaterighthindleg_y<<",";
      myfile1 <<t1.rotaterighthindleg_z<<",";

      /****rotate controller angles for fore leg****/
      myfile1 <<t1.rotaterightforeleg<<",";

      /****rotate controller angles for  neck****/
      myfile1 <<t1.rotateneck_x<<",";
      myfile1 <<t1.rotateneck_y<<",";
      myfile1 <<t1.rotateneck_z<<",";

      myfile1 <<t1.rotatechest_x<<",";
      myfile1 <<t1.rotatechest_y<<",";
      myfile1 <<t1.rotatechest_z<<",";

      myfile1 <<t1.bonescale_x<<",";
      myfile1 <<t1.bonescale_y<<",";
      myfile1 <<t1.bonescale_z<<",";

      myfile1 <<t1.modelscale_x<<",";
      myfile1 <<t1.modelscale_y<<",";
      myfile1 <<t1.modelscale_z<<",";

      // wheel rotate
      myfile1 <<t1.rotateLeftfWheel<<",";
      myfile1 <<t1.rotateLeftbWheel<<",";
      myfile1 <<t1.rotateRightfWheel<<",";
      myfile1 <<t1.rotateRightbWheel<<",";

     myfile1 <<rotate_x<<",";     
     myfile1 <<rotate_y<<",";     
     myfile1 <<trflag<<",";
     myfile1 <<camFlag<<",";
     myfile1 <<light1flag<<",";
     myfile1 <<light2flag<<",";
     myfile1 <<movecarforward<<",";
     myfile1 <<rotatecar_y<<",";
     myfile1 <<dir<<",";
     myfile1 <<tx<<",";
     myfile1 <<tz<<",";
     myfile1 <<lookatx<<",";
     myfile1 <<lookatz<<",";
     myfile1 <<camcfx<<",";
     myfile1 <<camcfz<<",";
     myfile1 <<camcx<<",";
     myfile1 <<camcz<<",";
     myfile1 <<cambx<<","; 
     myfile1 <<cambz<<",";
     myfile1 <<cambfx<<",";
     myfile1 <<cambfz;

      myfile1<<"\n";
      myfile1.close();
 }


 SDL_Surface* flipVert(SDL_Surface* sfc)
{
  SDL_Surface* result = SDL_CreateRGBSurface(sfc->flags, sfc->w, sfc->h,
    sfc->format->BytesPerPixel * 8, sfc->format->Rmask, sfc->format->Gmask,
    sfc->format->Bmask, sfc->format->Amask);
  if (result == NULL) return NULL;
 
  Uint8* pixels = (Uint8*) sfc->pixels;
  Uint8* rpixels = (Uint8*) result->pixels;
 
  Uint32 pitch = sfc->pitch;
  Uint32 pxlength = pitch*sfc->h;
 
  for(int line = 0; line < sfc->h; ++line) {
    Uint32 pos = line * pitch;
    memcpy(&rpixels[pos], &pixels[(pxlength-pos)-pitch], pitch);
  }
 
  return result;
}

void extrapolate(string temp1,string temp2){
  static int framecount= 1;
  float val1[65];
  float val2[65];
  int i=0;
  int me=0;
  char * S = new char[temp1.length() + 1];
  
  strcpy(S,temp1.c_str());
  char *tok = strtok(S, ",");
  while (tok != NULL) {
    val1[i++] = atof(tok);
    tok = strtok(NULL, ",");
  }

  
  // for (int j = 0; j < 65; ++j)
  // {
  //    code 
  //   cout<<val1[j]<<endl;
  // }
  // cout<<endl;

  char * S1 = new char[temp2.length() + 1];
  strcpy(S1,temp2.c_str());
  char *tok1 = strtok(S1, ",");
  while (tok1 != NULL) {
    val2[me++] = atof(tok1);
    tok1 = strtok(NULL, ",");

  }

  float framematrix[numberofframes][65];

  for (int j = 1; j <= numberofframes; ++j)
  {
    /* code */
    for (int k = 0; k < 65; ++k)
    {
      /* code */

      framematrix[j-1][k] = val1[k]+(((val2[k]-val1[k])/numberofframes)*j);
      cout<<framematrix[j-1][k]<<",";
    }
    cout<<endl;
  }
  glfwSetTime (0);
  int count =0;

  while(1){

    double curtime = (double)glfwGetTime();
    //cout<<curtime<<endl;
    if(curtime>=0.1/numberofframes*count){
      //setvaiable

       t1.rotatelefthindarm_x=framematrix[count][0];
      t1.rotatelefthindarm_y=framematrix[count][1];
      t1.rotatelefthindarm_z=framematrix[count][2];

      /****rotate controller angles for fore arm****/
      t1.rotateleftforearm=framematrix[count][3];

      /****rotate controller angles for hind arm****/
      t1.rotaterighthindarm_x=framematrix[count][4];
      t1.rotaterighthindarm_y=framematrix[count][5];
      t1.rotaterighthindarm_z=framematrix[count][6];

      /****rotate controller angles for fore arm****/
      t1.rotaterightforearm=framematrix[count][7];

      /****rotate controller angles for hind leg****/
      t1.rotatelefthindleg_x=framematrix[count][8];
      t1.rotatelefthindleg_y=framematrix[count][9];
      t1.rotatelefthindleg_z=framematrix[count][10];

      t1.rotateleftarmweapon_x=framematrix[count][11];
      t1.rotateleftarmweapon_y=framematrix[count][12];
      t1.rotateleftarmweapon_z=framematrix[count][13];

      t1.rotaterightarmweapon_x=framematrix[count][14];
      t1.rotaterightarmweapon_y=framematrix[count][15];
      t1.rotaterightarmweapon_z=framematrix[count][16];

      t1.rotateleftboot_x=framematrix[count][17];
      t1.rotateleftboot_y=framematrix[count][18];
      t1.rotateleftboot_z=framematrix[count][19];

      t1.rotaterightboot_x=framematrix[count][20];
      t1.rotaterightboot_y=framematrix[count][21];
      t1.rotaterightboot_z=framematrix[count][22];

      


      /****rotate controller angles for fore leg****/
      t1.rotateleftforeleg=framematrix[count][23];

      /****rotate controller angles for hind leg****/
      t1.rotaterighthindleg_x=framematrix[count][24];
      t1.rotaterighthindleg_y=framematrix[count][25];
      t1.rotaterighthindleg_z=framematrix[count][26];

      /****rotate controller angles for fore leg****/
      t1.rotaterightforeleg=framematrix[count][27];

      /****rotate controller angles for  neck****/
      t1.rotateneck_x=framematrix[count][28];
      t1.rotateneck_y=framematrix[count][29];
      t1.rotateneck_z=framematrix[count][30];

      t1.rotatechest_x=framematrix[count][31];
      t1.rotatechest_y=framematrix[count][32];
      t1.rotatechest_z=framematrix[count][33];

      t1.bonescale_x=framematrix[count][34];
      t1.bonescale_y=framematrix[count][35];
      t1.bonescale_z=framematrix[count][36];

      t1.modelscale_x=framematrix[count][37];
      t1.modelscale_y=framematrix[count][38];
      t1.modelscale_z=framematrix[count][39];

      // wheel rotate
      t1.rotateLeftfWheel=framematrix[count][40];

      cout<<t1.rotateLeftfWheel<<endl;

      t1.rotateLeftbWheel=framematrix[count][41];
      t1.rotateRightfWheel=framematrix[count][42];
      t1.rotateRightbWheel=framematrix[count][43];

     rotate_x=framematrix[count][44];     
     rotate_y=framematrix[count][45];     
     trflag=framematrix[count][46];
     camFlag=framematrix[count][47];
     light1flag=framematrix[count][48];
     light2flag=framematrix[count][49];
     movecarforward=framematrix[count][50];
     rotatecar_y=framematrix[count][51];
     dir=framematrix[count][52];

     tx=framematrix[count][53];
     tz=framematrix[count][54];
     lookatx=framematrix[count][55];
     lookatz=framematrix[count][56];
     camcfx=framematrix[count][57];
     camcfz=framematrix[count][58];
     camcx=framematrix[count][59];
     camcz=framematrix[count][60];
     cambx=framematrix[count][61]; 
     cambz=framematrix[count][62];
     cambfx=framematrix[count][63];
     cambfz=framematrix[count][64];

     count++;

    // sleep(0.1);
     //t1.makemodel();
     SDL_Surface * surf = SDL_CreateRGBSurface(SDL_SWSURFACE, win_width, win_height, 24, 0x000000FF, 0x0000FF00, 0x00FF0000, 0);
     if (surf == NULL) return;
     glReadPixels(0, 0, win_width, win_height, GL_RGB, GL_UNSIGNED_BYTE, surf->pixels);
     SDL_Surface * flip = flipVert(surf);
      if (flip == NULL) return;
      SDL_FreeSurface(surf);
      string filename = to_string(framecount);
      filename = filename+".bmp";
      framecount = framecount+1;
      SDL_SaveBMP(flip, filename.c_str());
      SDL_FreeSurface(flip);

     vector<uint8_t> data(win_width*win_height*4);
     glReadBuffer(GL_BACK);
     glReadPixels(0,0,win_width,win_height,GL_BGRA,GL_UNSIGNED_BYTE,&data[0]);
     renderGL();

     glfwSwapBuffers(window);

     saveframe1();

     if(count>numberofframes-1)
      break;


    }


  }

  // for (int j = 0; j < 65; ++j)
  // {
  //    code 
  //   cout<<val2[j]<<endl;
  // }
  // cout<<endl;

  // cout<<temp1<<endl;
  // cout<<temp2<<endl;


}


 void playback(){
    myfile.close();
    ifstream infile("keyframes.txt");
    string temp1;
    string temp2;
    getline(infile,temp1);
    while(getline(infile, temp2)) {
      //Do with temp
      extrapolate(temp1,temp2);
      temp1=temp2;
         
      glfwSwapBuffers(window);
    }
    myfile.open ("keyframes.txt",ios::app);

 }


/****************************Environment goes here********************************************************/


 


 
 typedef enum{
  none,
  neck,
  left_fore_leg,
  left_hind_leg,
  right_fore_leg,
  right_hind_leg,
  left_fore_arm,
  left_hind_arm,
  right_fore_arm,
  right_hind_arm,
  left_arm_weapon,
  right_arm_weapon,
  chest,
  left_boot,
  right_boot

 }keycontrollers;




 Environment env1;

keycontrollers keyctrl = none;



void renderGL(){
  //If a filename was given on the command line

glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
glClearDepth(1.0f);
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);




/***************************for environment ****************************************************/

  
  //glMatrixMode(GL_PROJECTION);
   
  //glMatrixMode(GL_MODELVIEW);
   
  glLoadIdentity();
  if(camFlag<1){
    gluPerspective(100, 1, 0.5, 1000);
    gluLookAt(0,-3.3,2.3, 0, -3.3, -1, 0, 1, 0);
  }

  if(camFlag>=1 && camFlag<1.5){
    


    gluPerspective(100, 1, 0.5, 1000);
    gluLookAt(cambx,-3.2, cambz, cambfx, -3.2, cambfz,0, 1, 0);
  }
  if(camFlag>1.5 && camFlag <=2){
    gluPerspective(100, 1, 0.5, 1000);
    gluLookAt(camcx,-4, camcz, camcfx, -4, camcfz,0, 1, 0);
  }  

  //gluLookAt(0, 10, 1.0, 0, 0, -3, 0, 1, 0);
  glPushMatrix();
  
  GLfloat ambientColor[] = {0.35f, 0.35f, 0.35f, 1.0f}; //Color(0.2, 0.2, 0.2)
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambientColor);
  /*****light 1******/
  GLfloat lightColor1[] = {1, 1, 1, 1.0f};
  GLfloat lightPos1[] = {1, 1, 1, 1.0f};
  //GLfloat spot_direction[] = {1.0, 0.0, 0.0};
  //glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, spot_direction);
  glLightf(GL_LIGHT1,GL_SPOT_CUTOFF,60.0f);
// Fairly shiny spot
glLightf(GL_LIGHT1,GL_SPOT_EXPONENT,100.0f);
   glLightfv(GL_LIGHT1, GL_DIFFUSE, lightColor1);
   glLightfv(GL_LIGHT1, GL_POSITION, lightPos1);
   //glEnable(GL_LIGHT1);
   if(light1flag==1){
    glEnable(GL_LIGHT1);
  }
  else
    glDisable(GL_LIGHT1);

   /*****light 2******/
  GLfloat lightColor2[] = {1, 1, 1, 1.0f};
  GLfloat lightPos2[] = {-1, 3, 15, 1.0f};
  glLightf(GL_LIGHT2,GL_SPOT_CUTOFF,60.0f);
// Fairly shiny spot
   glLightf(GL_LIGHT2,GL_SPOT_EXPONENT,100.0f);
   glLightfv(GL_LIGHT2, GL_DIFFUSE, lightColor2);
   glLightfv(GL_LIGHT2, GL_POSITION, lightPos2);
   if(light2flag==1){
    glEnable(GL_LIGHT2);
  }
  else
    glDisable(GL_LIGHT2);

   glScalef(5.0,5.0,5.0);
  //glRotatef( rotatecar_y, 0.0, 1.0, 0.0 );
  env1.drawenvironment();
   glPopMatrix();
 
  
  
   
  glPushMatrix();
  glRotatef( rotate_x, 1.0, 0.0, 0.0 );
  glRotatef( rotate_y, 0.0, 1.0, 0.0 );
    glTranslatef(0.0,-3.8,-0.5);
    glTranslatef(0,0,movecarforward*cos(dir*PI/180));
     glTranslatef(movecarforward*sin(dir*PI/180),0,0);
     glRotatef( 180, 0.0, 1.0, 0.0 );
     glRotatef( dir, 0.0, 1.0, 0.0 );
     //glRotatef( rotate_x, 1.0, 0.0, 0.0 );
    //glRotatef( rotate_y, 0.0, 1.0, 0.0 );

    glScalef(1,1,1);
     

    t1.makemodel();
  glPopMatrix();

  //glCallList(skyboxlist);


glFlush();

}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
  {
    //!Close the window if the ESC key was pressed
    if (key == GLFW_KEY_ESCAPE  && action == GLFW_PRESS)
      glfwSetWindowShouldClose(window, GL_TRUE);
    if (key == GLFW_KEY_RIGHT && keyctrl==none && action == GLFW_PRESS){
       dir-=5;
            float s = sin(5*PI/180);
            float c = cos(5*PI/180);

      //   lookatx -= tx;
      //   lookatz -= -0.98+tz;

      //   // rotate point
      //   float xnew = lookatx * c - lookatz * s;
      //   float znew = lookatx * s + lookatz * c;

      //   // translate point back:
      //   lookatx = xnew + tx;
      //   lookatz = znew + -0.98+tz;
      // // camera following
      //   s = sin(5*PI/180);
      //   c = cos(5*PI/180);



        float midbx;
        float midbz;
        midbx=(cambx+cambfx)/2;
        midbz=(cambz+cambfz)/2;
        cambfx=cambfx-midbx;
        cambfz=cambfz-midbz;
        float bfxn=cambfx * c - cambfz * s;
        float bfxz=cambfx * s + cambfz * c;
        cambfx = bfxn+ midbx;
        cambfz = bfxz + midbz;



        cambx = cambx - midbx;
        cambz = cambz - midbz;

        float bxn= cambx * c - cambz * s;
        float bxz= cambx * s + cambz * c;

        cambx = bxn + midbx;
        cambz = bxz + midbz;



        float midx;
        float midz;
        midx=(camcx+camcfx)/2;
        midz=(camcz+camcfz)/2;
        camcfx=camcfx-midx;
        camcfz=camcfz-midz;
        float cfxn=camcfx * c - camcfz * s;
        float cfxz=camcfx * s + camcfz * c;
        camcfx = cfxn + midx;
        camcfz = cfxz + midz;

        camcx = camcx - midx;
        camcz = camcz - midz;

        float cxn= camcx * c - camcz * s;
        float cxz= camcx * s + camcz * c;

        camcx = cxn + midx;
        camcz = cxz + midz;

        t1.rotateLeftfWheel=10;
        
        t1.rotateRightfWheel=10;
    }
    if (key == GLFW_KEY_RIGHT && keyctrl==none && action == GLFW_RELEASE){
      t1.rotateLeftfWheel=0;
      
      t1.rotateRightfWheel=0;

        }
    if (key == GLFW_KEY_LEFT && keyctrl==none && action == GLFW_PRESS){
            dir+=5;
            float s = sin(-((5/1.35)*PI)/180);
            float c = cos(-5/1.35*PI/180);

        lookatx -= tx;
        lookatz -= -0.98+tz;

        // rotate point
        float xnew = lookatx * c - lookatz * s;
        float znew = lookatx * s + lookatz * c;

        // translate point back:
        lookatx = xnew + tx;
        lookatz = znew + -0.98+tz;
      // camera following
        s = sin(-5*PI/180);
        c = cos(-5*PI/180);

        float midbx;
        float midbz;
        midbx=(cambx+cambfx)/2;
        midbz=(cambz+cambfz)/2;
        cambfx=cambfx-midbx;
        cambfz=cambfz-midbz;
        float bfxn=cambfx * c - cambfz * s;
        float bfxz=cambfx * s + cambfz * c;
        cambfx = bfxn+ midbx;
        cambfz = bfxz + midbz;



        cambx = cambx - midbx;
        cambz = cambz - midbz;

        float bxn= cambx * c - cambz * s;
        float bxz= cambx * s + cambz * c;

        cambx = bxn + midbx;
        cambz = bxz + midbz;




        float midx;
        float midz;
        midx=(camcx+camcfx)/2;
        midz=(camcz+camcfz)/2;
        camcfx=camcfx-midx;
        camcfz=camcfz-midz;
        float cfxn=camcfx * c - camcfz * s;
        float cfxz=camcfx * s + camcfz * c;
        camcfx = cfxn + midx;
        camcfz = cfxz + midz;

        camcx = camcx - midx;
        camcz = camcz - midz;

        float cxn= camcx * c - camcz * s;
        float cxz= camcx * s + camcz * c;

        camcx = cxn + midx;
        camcz = cxz + midz;

        t1.rotateLeftfWheel=-10;
        // t1.rotateLeftbWheel=-10;
         //t1.rotateRightbWheel=-10;
         t1.rotateRightfWheel=-10;
    }
    if (key == GLFW_KEY_LEFT && keyctrl==none && action == GLFW_RELEASE){
      t1.rotateLeftfWheel=0;
      
      t1.rotateRightfWheel=0;

        }
    if (key == GLFW_KEY_UP && keyctrl==none && action == GLFW_PRESS){
      movecarforward+=0.02;
       tz+=0.02*cos(dir*PI/180);
       tx+=0.02*sin(dir*PI/180);
       camcfx+=0.02*sin(dir*PI/180);
       camcfz+=0.02*cos(dir*PI/180);
       camcx+=0.02*sin(dir*PI/180);
       camcz+=0.02*cos(dir*PI/180);
       /////////////////////////////////////////////////////
       cambfx+=0.06*sin(dir*PI/180);
       cambfz+=0.06*cos(dir*PI/180);
       cambx+=0.02*sin(dir*PI/180);
       cambz+=0.02*cos(dir*PI/180);

       t1.rotateLeftfWheel=-2;
      t1.rotateLeftbWheel=-2;
      t1.rotateRightbWheel=-2;
      t1.rotateRightfWheel=-2;
            
    }
    if (key == GLFW_KEY_UP && keyctrl==none && action == GLFW_RELEASE){

      t1.rotateLeftfWheel=0;
      t1.rotateLeftbWheel=0;
      t1.rotateRightbWheel=0;
      t1.rotateRightfWheel=0;
}


if (key == GLFW_KEY_DOWN && keyctrl==none && action == GLFW_RELEASE){
      t1.rotateLeftfWheel=0;
      t1.rotateLeftbWheel=0;
      t1.rotateRightbWheel=0;
      t1.rotateRightfWheel=0;
    }

    if (key == GLFW_KEY_DOWN && keyctrl==none && action == GLFW_PRESS){
     movecarforward-=0.02;
       tz-=0.02*cos(dir*PI/180);
       tx-=0.02*sin(dir*PI/180);
       camcfx-=0.02*sin(dir*PI/180);
       camcfz-=0.02*cos(dir*PI/180);
       camcx-=0.02*sin(dir*PI/180);
       camcz-=0.02*cos(dir*PI/180);
       /////////////////////////////////////////////////////
       cambfx-=0.06*sin(dir*PI/180);
       cambfz-=0.06*cos(dir*PI/180);
       cambx-=0.02*sin(dir*PI/180);
       cambz-=0.02*cos(dir*PI/180);

      t1.rotateLeftfWheel=-2;
      t1.rotateLeftbWheel=-2;
      t1.rotateRightbWheel=-2;
      t1.rotateRightfWheel=-2;
    }


    if(key == GLFW_KEY_0 && action == GLFW_PRESS){
        keyctrl=none;
    }

    if(key == GLFW_KEY_Y && action == GLFW_PRESS){
        keyctrl=neck;
    }

    if(key == GLFW_KEY_G && action == GLFW_PRESS){
        keyctrl=left_hind_arm;
    }

    if(key == GLFW_KEY_H && action == GLFW_PRESS){
        keyctrl=chest;
    }
    if(key == GLFW_KEY_V && action == GLFW_PRESS){
        keyctrl=left_fore_arm;
    }
    if(key == GLFW_KEY_J && action == GLFW_PRESS){
        keyctrl=right_hind_arm;
    }
    if(key == GLFW_KEY_M && action == GLFW_PRESS){
        keyctrl=right_fore_arm;
    }
    if(key == GLFW_KEY_I && action == GLFW_PRESS){
        keyctrl=left_hind_leg;
    }
    if(key == GLFW_KEY_K && action == GLFW_PRESS){
        keyctrl=left_fore_leg;
    }
    if(key == GLFW_KEY_O && action == GLFW_PRESS){
        keyctrl=right_hind_leg;
    }
     if(key == GLFW_KEY_L && action == GLFW_PRESS){
        keyctrl=right_fore_leg;
    }
    if(key == GLFW_KEY_B && action == GLFW_PRESS){
        keyctrl=left_arm_weapon;
    }
    if(key == GLFW_KEY_N && action == GLFW_PRESS){
        keyctrl=right_arm_weapon;
    }

    if(key == GLFW_KEY_Z && action == GLFW_PRESS){
        keyctrl=right_boot;
    }
    if(key == GLFW_KEY_X && action == GLFW_PRESS){
        keyctrl=left_boot;
    }
    
    
     if(key == GLFW_KEY_F && action == GLFW_PRESS){
      if(trflag==0){
          t1.rotaterighthindleg_x=-70;
          t1.rotatelefthindleg_x=-70;
          t1.rotatechest_x = -100;
          t1.rotatechest_y = 180;
          t1.rotaterighthindarm_x= 30;
          t1.rotatelefthindarm_x = 45;
          //rotate_y = 100;
           //rotate_x = -5;
          t1.rotateneck_x = -70;
          trflag=1;
          }
       else{
          t1.rotaterighthindleg_x=0;
          t1.rotatelefthindleg_x=0;
          t1.rotatechest_x = 0;
          t1.rotatechest_y = 0;
          t1.rotaterighthindarm_x= 0;
          t1.rotatelefthindarm_x = 0;
          t1.rotateneck_x = 0;
         // rotate_y=0;
         // rotate_x=0;
          trflag=0;
        }
    }
    
    
   
// boot rotation

     if (key == GLFW_KEY_1 && action == GLFW_PRESS){
      if(light1flag==1)
        light1flag=0;
      else
        light1flag = 1;
    }
    if (key == GLFW_KEY_2 && action == GLFW_PRESS){
      if(light2flag==1)
        light2flag=0;
      else
        light2flag = 1;
    }

     if (key == GLFW_KEY_A && keyctrl==right_boot && action == GLFW_PRESS){
      if(t1.rotaterightboot_x>=-30)
        t1.rotaterightboot_x-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D && keyctrl==right_boot && action == GLFW_PRESS){
      if(t1.rotaterightboot_x<=30)
        t1.rotaterightboot_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_W &&  keyctrl==right_boot && action == GLFW_PRESS){
      if(t1.rotaterightboot_y<=25)
          t1.rotaterightboot_y+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_S && keyctrl==right_boot && action == GLFW_PRESS){
      if(t1.rotaterightboot_y>=-25)
          t1.rotaterightboot_y-= 5;
      glfwSwapBuffers(window);
    }


    //left boot rotation
     if (key == GLFW_KEY_A && keyctrl==left_boot && action == GLFW_PRESS){
      if(t1.rotateleftboot_x>=-30)
        t1.rotateleftboot_x-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D && keyctrl==left_boot && action == GLFW_PRESS){
      if(t1.rotateleftboot_x<=30)
        t1.rotateleftboot_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_W &&  keyctrl==left_boot && action == GLFW_PRESS){
      if(t1.rotateleftboot_y<=25)
          t1.rotateleftboot_y+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_S && keyctrl==left_boot && action == GLFW_PRESS){
      if(t1.rotateleftboot_y>=-25)
          t1.rotateleftboot_y-= 5;
      glfwSwapBuffers(window);
    }




    // neck rotation


    if (key == GLFW_KEY_Q && keyctrl==neck && action == GLFW_PRESS){
      if(t1.rotateneck_z>=-15)
        t1.rotateneck_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==neck && action == GLFW_PRESS){
      if(t1.rotateneck_z<=15)
        t1.rotateneck_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A && keyctrl==neck && action == GLFW_PRESS){
      if(t1.rotateneck_x>=-30)
        t1.rotateneck_x-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D && keyctrl==neck && action == GLFW_PRESS){
      if(t1.rotateneck_x<=30)
        t1.rotateneck_x+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_W &&  keyctrl==neck && action == GLFW_PRESS){
      if(t1.rotateneck_y<=25)
          t1.rotateneck_y+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_S && keyctrl==neck && action == GLFW_PRESS){
      if(t1.rotateneck_y>=-25)
          t1.rotateneck_y-= 5;
      glfwSwapBuffers(window);
    }


    if (key == GLFW_KEY_A && keyctrl==left_fore_leg && action == GLFW_PRESS){
      
      if(t1.rotateleftforeleg<=30)
          t1.rotateleftforeleg+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D &&  keyctrl==left_fore_leg && action == GLFW_PRESS){
      if(t1.rotateleftforeleg>=-30)
          t1.rotateleftforeleg-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_A && keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(t1.rotatelefthindleg_x<=60)
          t1.rotatelefthindleg_x+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D &&  keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(t1.rotatelefthindleg_x>=-60)
          t1.rotatelefthindleg_x-= 5;
      glfwSwapBuffers(window);
    }
     if (key == GLFW_KEY_E &&  keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(t1.rotatelefthindleg_z<=30)
          t1.rotatelefthindleg_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_Q &&  keyctrl==left_hind_leg && action == GLFW_PRESS){
      if(t1.rotatelefthindleg_z>=-30)
          t1.rotatelefthindleg_z-= 5;
      glfwSwapBuffers(window);
    }

//right leg start from here

    if (key == GLFW_KEY_D &&  keyctrl==right_fore_leg && action == GLFW_PRESS){
      if(t1.rotaterightforeleg<=30)
          t1.rotaterightforeleg+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A && keyctrl==right_fore_leg && action == GLFW_PRESS){
      if(t1.rotaterightforeleg>=-30)
          t1.rotaterightforeleg-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D &&  keyctrl==right_hind_leg && action == GLFW_PRESS){
      if(t1.rotaterighthindleg_x<=60)
          t1.rotaterighthindleg_x+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A  &&  keyctrl==right_hind_leg  && action == GLFW_PRESS){
      if(t1.rotaterighthindleg_x>=-60)
          t1.rotaterighthindleg_x-= 5;
      glfwSwapBuffers(window);
    }
     if (key == GLFW_KEY_E  &&  keyctrl==right_hind_leg  && action == GLFW_PRESS){
      if(t1.rotaterighthindleg_z<=30)
          t1.rotaterighthindleg_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_Q  &&  keyctrl==right_hind_leg  && action == GLFW_PRESS){
      if(t1.rotaterighthindleg_z>=-30)
          t1.rotaterighthindleg_z-= 5;
      glfwSwapBuffers(window);
    }

    //chest start from here
    if (key == GLFW_KEY_W &&  keyctrl==chest && action == GLFW_PRESS){
      if(t1.rotatechest_y<=175)
          t1.rotatechest_y+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_S && keyctrl==chest && action == GLFW_PRESS){
      if(t1.rotatechest_y>=-175)
          t1.rotatechest_y-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==chest && action == GLFW_PRESS){
      if(t1.rotatechest_x<=90)
          t1.rotatechest_x+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_A && keyctrl==chest && action == GLFW_PRESS){
      if(t1.rotatechest_x>=-40)
          t1.rotatechest_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_E && keyctrl==chest && action == GLFW_PRESS){
      if(t1.rotatechest_z<=30)
          t1.rotatechest_z+= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_Q && keyctrl==chest && action == GLFW_PRESS){
      if(t1.rotatechest_z>=-30)
          t1.rotatechest_z-= 5;
      glfwSwapBuffers(window);
    }


    //making the arms


     if (key == GLFW_KEY_A && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(t1.rotatelefthindarm_x>=-70)
          t1.rotatelefthindarm_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(t1.rotatelefthindarm_x<=70)
          t1.rotatelefthindarm_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_S && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(t1.rotatelefthindarm_y>=-30)
          t1.rotatelefthindarm_y-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_W && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(t1.rotatelefthindarm_y<=30)
          t1.rotatelefthindarm_y+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_Q && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(t1.rotatelefthindarm_z>=-130)
          t1.rotatelefthindarm_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==left_hind_arm && action == GLFW_PRESS){
      if(t1.rotatelefthindarm_z<=0)
          t1.rotatelefthindarm_z+= 5;
      glfwSwapBuffers(window);
    }
//making the left arm weapn/////////////////////////////////////////////////////////////////////////////////////

     if (key == GLFW_KEY_A && keyctrl==left_arm_weapon && action == GLFW_PRESS){
      if(t1.rotateleftarmweapon_x>=-30)
          t1.rotateleftarmweapon_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==left_arm_weapon && action == GLFW_PRESS){
      if(t1.rotateleftarmweapon_x<=30)
          t1.rotateleftarmweapon_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_S && keyctrl==left_arm_weapon && action == GLFW_PRESS){
      if(t1.rotateleftarmweapon_y>=-30)
          t1.rotateleftarmweapon_y-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_W && keyctrl==left_arm_weapon && action == GLFW_PRESS){
      if(t1.rotateleftarmweapon_y<=30)
          t1.rotateleftarmweapon_y+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_Q && keyctrl==left_arm_weapon && action == GLFW_PRESS){
      if(t1.rotateleftarmweapon_z>=-30)
          t1.rotateleftarmweapon_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==left_arm_weapon && action == GLFW_PRESS){
      if(t1.rotateleftarmweapon_z<=30)
          t1.rotateleftarmweapon_z+= 5;
      glfwSwapBuffers(window);
    }


    /////////////////////////////////////////////////////////////////////////////////////////

    //making the right arm weapn/////////////////////////////////////////////////////////////////////////////////////

     if (key == GLFW_KEY_A && keyctrl==right_arm_weapon && action == GLFW_PRESS){
      if(t1.rotaterightarmweapon_x>=-45)
          t1.rotaterightarmweapon_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==right_arm_weapon && action == GLFW_PRESS){
      if(t1.rotaterightarmweapon_x<=45)
          t1.rotaterightarmweapon_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_S && keyctrl==right_arm_weapon && action == GLFW_PRESS){
      if(t1.rotaterightarmweapon_y>=-45)
          t1.rotaterightarmweapon_y-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_W && keyctrl==right_arm_weapon && action == GLFW_PRESS){
      if(t1.rotaterightarmweapon_y<=45)
          t1.rotaterightarmweapon_y+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_Q && keyctrl==right_arm_weapon && action == GLFW_PRESS){
      if(t1.rotaterightarmweapon_z>=-45)
          t1.rotaterightarmweapon_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==right_arm_weapon && action == GLFW_PRESS){
      if(t1.rotaterightarmweapon_z<=45)
          t1.rotaterightarmweapon_z+= 5;
      glfwSwapBuffers(window);
    }


    /////////////////////////////////////////////////////////////////////////////////////////

     if (key == GLFW_KEY_A && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(t1.rotaterighthindarm_x>=-70)
          t1.rotaterighthindarm_x-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(t1.rotaterighthindarm_x<=70)
          t1.rotaterighthindarm_x+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_S && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(t1.rotaterighthindarm_y>=-30)
          t1.rotaterighthindarm_y-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_W && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(t1.rotaterighthindarm_y<=30)
          t1.rotaterighthindarm_y+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_Q && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(t1.rotaterighthindarm_z>=0)
          t1.rotaterighthindarm_z-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_E && keyctrl==right_hind_arm && action == GLFW_PRESS){
      if(t1.rotaterighthindarm_z<=150)
          t1.rotaterighthindarm_z+= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_A && keyctrl==left_fore_arm && action == GLFW_PRESS){
      if(t1.rotateleftforearm>=-140)
          t1.rotateleftforearm-= 5;
      glfwSwapBuffers(window);
    }
    if (key == GLFW_KEY_D && keyctrl==left_fore_arm && action == GLFW_PRESS){
      if(t1.rotateleftforearm<=0)
          t1.rotateleftforearm+= 5;
      glfwSwapBuffers(window);
    }

     if (key == GLFW_KEY_A && keyctrl==right_fore_arm && action == GLFW_PRESS){
      if(t1.rotaterightforearm>=-140)
          t1.rotaterightforearm-= 5;
      glfwSwapBuffers(window);
    }

    if (key == GLFW_KEY_D && keyctrl==right_fore_arm && action == GLFW_PRESS){
      if(t1.rotaterightforearm<=0)
          t1.rotaterightforearm+= 5;
      glfwSwapBuffers(window);
    }

// rotate wheel 
if (key == GLFW_KEY_3 && action == GLFW_PRESS){
       t1.rotateLeftfWheel=30;
       t1.rotateLeftbWheel=30;
       t1.rotateRightbWheel=30;
       t1.rotateRightfWheel=30;
  }

if (key == GLFW_KEY_7 && action == GLFW_RELEASE){
  t1.rotateLeftfWheel=0;
  t1.rotateRightfWheel=0;
  t1.rotateLeftbWheel=0;
  t1.rotateRightbWheel=0;

}

  if (key == GLFW_KEY_8 && action == GLFW_RELEASE){
    usleep(1000);
    t1.rotateLeftfWheel=0;
   // t1.rotateLeftbWheel=0;
   // t1.rotateRightbWheel=0;
    t1.rotateRightfWheel=0;
  }

// camera toggle key
if(key == GLFW_KEY_6 && action == GLFW_PRESS){
        if(camFlag==0)
            camFlag=1;
          else if(camFlag==1)
                camFlag=2;
              else if(camFlag==2)
                    camFlag=0;
    }

  if (key == GLFW_KEY_LEFT_CONTROL && action == GLFW_PRESS){
      saveframe();
      
    } 

    if (key == GLFW_KEY_LEFT_ALT && action == GLFW_PRESS){
      
      myfile.close();
      
    } 

  if (key == GLFW_KEY_SPACE && action == GLFW_PRESS){
      playback();
      
  }   


  }

int main (int argc, char *argv[]) 
{
  //progname=argv[0];

  //! The pointer to the GLFW window
  
  
  //glutInit(&argc,argv);

  //! Setting up the GLFW Error callback
 
  myfile.open ("keyframes.txt",ios::out);
  glfwSetErrorCallback(csX75::error_callback);

  //! Initialize GLFW
  if (!glfwInit())
    return -1;

  //setup(argc, argv);

  
  //! Create a windowed mode window and its OpenGL context
  window = glfwCreateWindow(win_width, win_height, "Transformers", NULL, NULL);
  if (!window)
    {
      glfwTerminate();
      return -1;
    }
  
  //! Make the window's context current 
  glfwMakeContextCurrent(window);

  //Keyboard Callback
  glfwSetKeyCallback(window, key_callback);
  //Framebuffer resize callback
  glfwSetFramebufferSizeCallback(window, csX75::framebuffer_size_callback);

  // Ensure we can capture the escape key being pressed below
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

  glfwGetFramebufferSize(window, &win_width, &win_height);
  csX75::framebuffer_size_callback(window, win_width, win_height);
  //Initialize GL state
  csX75::initGL();
  glEnable(GL_DEPTH_TEST);
  env1.envinit();
  t1.init();
  // Loop until the user closes the window
  while (glfwWindowShouldClose(window) == 0)
    {
       
      // Render here
      //usleep(100000);
      
       
      renderGL();

      // Swap front and back buffers
      glfwSwapBuffers(window);
      
      // Poll for and process events
      glfwPollEvents();
    }

  glfwDestroyWindow(window);
   myfile.close();
  glfwTerminate();
  return 0;
}